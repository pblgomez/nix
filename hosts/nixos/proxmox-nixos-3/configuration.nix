{ lib, sshAuthorizedKeys, ... }: {
  boot.initrd.availableKernelModules =
    [ "ata_piix" "uhci_hcd" "virtio_pci" "virtio_scsi" "sd_mod" "sr_mod" ];

  boot.loader.grub.enable = true;

  users = {
    mutableUsers = false;
    users."root".openssh.authorizedKeys.keys = sshAuthorizedKeys;
  };

  services = {
    openssh = {
      enable = true;
      settings.PasswordAuthentication = false;
      settings.KbdInteractiveAuthentication = false;
      authorizedKeysFiles =
        lib.mkForce [ "%h/.ssh/authorized_keys /etc/ssh/authorized_keys.d/%u" ];
    };
  };
}
