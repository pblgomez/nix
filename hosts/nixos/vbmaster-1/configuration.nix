{ config, lib, sshAuthorizedKeys, ... }: {
  boot.initrd.availableKernelModules =
    [ "ata_piix" "ohci_pci" "ehci_pci" "ahci" "sd_mod" "sr_mod" ];
  boot.loader.grub.enable = true;

  users = {
    mutableUsers = false;
    users."root".openssh.authorizedKeys.keys = sshAuthorizedKeys;
  };

  services = {
    openssh = {
      enable = true;
      settings.PasswordAuthentication = false;
      settings.KbdInteractiveAuthentication = false;
      authorizedKeysFiles =
        lib.mkForce [ "%h/.ssh/authorized_keys /etc/ssh/authorized_keys.d/%u" ];
    };
  };

  systemd.services = {
    sshKeyPublic = {
      description = "Create public ssh host key from private";
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = "yes";
      };
      after = [ "multi-user.target" ];
      wantedBy = [ "multi-user.target" ];
      script = ''
        source ${config.system.build.setEnvironment}
        chmod 0600 /etc/ssh/ssh_host_ed25519_key
        ssh-keygen -e -f /etc/ssh/ssh_host_ed25519_key >/etc/ssh/ssh_host_ed25519_key.pub
      '';
    };
  };
  virtualisation.virtualbox.guest.enable = true;
}
