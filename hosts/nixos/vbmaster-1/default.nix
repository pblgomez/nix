{ inputs, lib, ... }: {
  imports = [
    inputs.agenix.nixosModules.default
    inputs.disko.nixosModules.default
    ./disko.nix

    ./configuration.nix
    ../../../nixosModules/common
    ../../../nixosModules/server
  ];

  myNixOS.k3s.enable = true;
  services.k3s.role = lib.mkForce "server";
  # services.k3s.clusterInit = lib.mkForce true;

  networking = { hostName = "vbmaster-1"; };

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  system.stateVersion = "23.11";
}
