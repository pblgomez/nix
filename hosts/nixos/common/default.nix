{ lib, ... }: {
  # boot.loader.systemd-boot.enable = true;
  # boot.loader.efi.canTouchEfiVariables = true;
  #
  # environment.systemPackages = with pkgs; [
  #   wl-clipboard
  # ];
  #
  # fonts.packages = with pkgs; [(nerdfonts.override {fonts = ["FiraCode" "Iosevka"];})];

  networking.networkmanager.enable = true;

  nix.gc.dates = "weekly";

  # users.users.${username} = {
  #   isNormalUser = true;
  #   initialPassword = "testtest";
  #   extraGroups = ["networkmanager" "wheel"];
  #   shell = pkgs.zsh;
  # };
  # services.openssh.enable = true;
  services = {
    openssh = {
      enable = true;
      settings.PermitRootLogin = "yes";
      authorizedKeysFiles =
        lib.mkForce [ "%h/.ssh/authorized_keys /etc/ssh/authorized_keys.d/%u" ];
    };
  };
}
