{ lib, pkgs, inputs, ... }:
let
  tailscaleToken =
    lib.removeSuffix "\n" (builtins.readFile /tmp/scrts/tailscaleToken);
  unstable = import inputs.nixpkgs-unstable { system = pkgs.system; };
in {
  services.tailscale = {
    enable = true;
    package = unstable.tailscale;
    useRoutingFeatures = "server";
  };
  systemd.services.tailscale-autoconnect = {
    description = "Automatic connection to Tailscale";

    # make sure tailscale is running before trying to connect to tailscale
    after = [ "network-pre.target" "run-agenix.d.mount" "tailscale.service" ];
    wants = [ "network-pre.target" "run-agenix.d.mount" "tailscale.service" ];
    wantedBy = [ "multi-user.target" ];

    # set this service as a oneshot job
    serviceConfig.Type = "oneshot";

    # have the job run this shell script
    script = with pkgs; ''
      # wait for tailscaled to settle
      sleep 2

      # check if we are already authenticated to tailscale
      status="$(${tailscale}/bin/tailscale status -json | ${jq}/bin/jq -r .BackendState)"
      if [ $status = "Running" ]; then # if so, then do nothing
        exit 0
      fi

      # otherwise authenticate with tailscale
      ${tailscale}/bin/tailscale up -authkey "${tailscaleToken}" --advertise-exit-node --advertise-routes=192.168.99.0/24,192.168.2.0/24
    '';
  };
}
