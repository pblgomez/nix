{ lib, sshAuthorizedKeys, ... }: {

  boot.loader.grub.enable = true;

  users = {
    mutableUsers = false;
    users."root".openssh.authorizedKeys.keys = sshAuthorizedKeys;
  };

  services = {
    openssh = {
      enable = true;
      authorizedKeysFiles =
        lib.mkForce [ "%h/.ssh/authorized_keys /etc/ssh/authorized_keys.d/%u" ];
    };
    udev.extraRules = ''
      KERNEL=="ttyUSB*", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", SYMLINK+="zigbee1"
    '';
  };
}
