{ inputs, lib, pkgs, sshAuthorizedKeys, username, ... }: {
  imports = [
    inputs.agenix.nixosModules.default
    inputs.disko.nixosModules.disko
    inputs.nixos-hardware.nixosModules.common-gpu-intel
    inputs.nixos-hardware.nixosModules.common-pc-ssd
    inputs.srvos.nixosModules.desktop
  ];

  age.secrets = {
    awsCreds = {
      file = ../../../secrets/awsCreds.age;
      path = "/home/${username}/.config/aws/credentials";
      mode = "400";
      owner = "${username}";
      group = "users";
    };
    sshPublicKey = {
      file = ../../../secrets/sshPublicKey.age;
      path = "/home/${username}/.ssh/id_ed25519.pub";
      mode = "400";
      owner = "${username}";
      group = "users";
    };
    sshKey = {
      file = ../../../secrets/sshKey-n100-1.age;
      path = "/home/${username}/.ssh/id_ed25519";
      mode = "400";
      owner = "${username}";
      group = "users";
    };
  };
  boot = {
    loader = {
      timeout = 1;
      efi.canTouchEfiVariables = true;
      systemd-boot.enable = true;
    };
  };

  environment.systemPackages = with pkgs; [ bitwarden slack ];

  networking = {
    hostName = "n100-1-desktop";
    useDHCP = lib.mkForce true;
  };

  nixpkgs.config.allowUnfree = true;

  # KDE
  services.xserver.enable = true;
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.desktopManager.plasma5.enable = true;
  services.xserver.displayManager.defaultSession = "plasmawayland";

  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
    settings.PermitRootLogin = "yes";
  };

  users.users."root".openssh.authorizedKeys.keys = sshAuthorizedKeys;

  system.stateVersion = "23.11";
}
