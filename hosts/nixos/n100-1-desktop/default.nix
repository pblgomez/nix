{ inputs, ... }: {
  imports = [
    ../../common
    ../common
    ../common/tailscale.nix
    ../n100-1/hardware-configuration.nix
    ../n100-1/disko.nix
    ./configuration.nix
    inputs.home-manager.nixosModules.home-manager
    {
      home-manager = {
        extraSpecialArgs = { inherit inputs; };
        users."pbl" = { imports = [ ../../../home/nixos.nix ]; };
      };
    }
  ];

  system.stateVersion = "23.11";
}
