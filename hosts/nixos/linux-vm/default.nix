{ inputs, lib, pkgs, sshAuthorizedKeys, ... }:
let username = "pbl";
in {
  imports = [
    ../common
    ../../common
    inputs.home-manager.nixosModules.home-manager
    {
      home-manager = {
        extraSpecialArgs = { inherit inputs; };
        users.${username} = { imports = [ ../../../home/pbl ]; };
      };
    }
  ];

  boot.loader.systemd-boot.enable = true;

  fileSystems."/" = {
    device = "/dev/disk/by-label/nixos";
    fsType = "ext4";
  };

  users.users."${username}" = {
    isNormalUser = true;
    initialPassword = username;
    extraGroups = [ "networkmanager" "wheel" ];
    openssh.authorizedKeys.keys = sshAuthorizedKeys;
    shell = pkgs.zsh;
  };
  programs.zsh.enable = true;

  services.qemuGuest.enable = true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  system.stateVersion = "23.11";
}
