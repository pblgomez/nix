{ inputs, lib, ... }: {
  imports = [
    inputs.agenix.nixosModules.default

    inputs.disko.nixosModules.default
    ../common/disko.nix

    ./configuration.nix
    ./hardware-configuration.nix
    ../../../nixosModules/common
    ../../../nixosModules/server
  ];

  myNixOS.k3s.enable = true;
  services.k3s.role = lib.mkForce "server";
  services.k3s.extraFlags =
    toString [ "--disable=traefik --tls-san=192.168.99.250" ]; # For servers
  myNixOS.tailscale.enable = true;

  networking = { hostName = "proxmox-nixos-master-3"; };

  system.stateVersion = "24.05";
}
