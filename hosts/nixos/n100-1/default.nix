{ lib, inputs, ... }: {
  imports = [
    inputs.agenix.nixosModules.default
    inputs.disko.nixosModules.default
    ./disko.nix

    inputs.nixos-hardware.nixosModules.common-gpu-intel-disable
    inputs.nixos-hardware.nixosModules.common-pc-ssd
    ./configuration.nix
    ../../../nixosModules/common
    ../../../nixosModules/server
  ];

  myNixOS.k3s.enable = true;
  services.k3s.role = lib.mkForce "server";

  networking = { hostName = "n100-1"; };

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  system.stateVersion = "23.11";
}
