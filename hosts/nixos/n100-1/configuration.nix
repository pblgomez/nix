{ config, inputs, lib, modulesPath, sshAuthorizedKeys, ... }:
let
  mqtt-password =
    lib.removeSuffix "\n" (builtins.readFile /tmp/scrts/mqtt-password);
in {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
    inputs.nixos-hardware.nixosModules.common-gpu-intel-disable
    inputs.nixos-hardware.nixosModules.common-pc-ssd
    inputs.srvos.nixosModules.server
  ];

  age.secrets = {
    awsCreds = {
      file = ../../../secrets/mqtt-password.age;
      path = "/tmp/scrts/mqtt-password";
      mode = "400";
    };
  };

  boot = {
    initrd.availableKernelModules = [ "xhci_pci" "ahci" "usbhid" "sd_mod" ];
    kernelModules = [ "kvm-intel" ];
    loader = {
      timeout = 1;
      efi.canTouchEfiVariables = true;
      systemd-boot.enable = true;
    };
  };

  hardware.cpu.intel.updateMicrocode =
    lib.mkDefault config.hardware.enableRedistributableFirmware;

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersafe";

  networking = {
    # firewall.allowedTCPPorts = [
    #   1883 # MQTT
    #   2379 # etcd
    #   2380 # etcd
    #   6443 # k8s
    # ];
    hostName = "n100-1";
    useDHCP = lib.mkForce true;
  };
  # networking.firewall.trustedInterfaces = ["enp1s0" "lo0" "tailscale0"];
  networking.firewall.enable = lib.mkForce false;
  # networking.firewall.allowedUDPPorts = [
  #   8472 # k3s, flannel: required if using multi-node for inter-node networking
  # ];

  users = {
    mutableUsers = false;
    users."root".openssh.authorizedKeys.keys = sshAuthorizedKeys;
  };

  services = {
    fstrim.enable = true;
    openssh = {
      enable = true;
      settings.PasswordAuthentication = false;
      settings.KbdInteractiveAuthentication = false;
      authorizedKeysFiles =
        lib.mkForce [ "%h/.ssh/authorized_keys /etc/ssh/authorized_keys.d/%u" ];
    };
    udev.extraRules = ''
      KERNEL=="ttyUSB*", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", SYMLINK+="zigbee1"
    '';
    zigbee2mqtt = {
      enable = true;
      settings = {
        homeassistant = true;
        # homeassistant = config.services.home-assistant.enable;
        mqtt = {
          server = "mqtt://192.168.99.250:1883";
          user = "nina";
          password = mqtt-password;
        };
        serial = { port = "/dev/zigbee1"; };
        frontend = true;
      };
    };
  };

  systemd.services = {
    sshKeyPublic = {
      description = "Create public ssh host key from private";
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = "yes";
      };
      after = [ "multi-user.target" ];
      wantedBy = [ "multi-user.target" ];
      script = ''
        source ${config.system.build.setEnvironment}
        chmod 0600 /etc/ssh/ssh_host_ed25519_key
        ssh-keygen -e -f /etc/ssh/ssh_host_ed25519_key >/etc/ssh/ssh_host_ed25519_key.pub
      '';
    };
  };
}
