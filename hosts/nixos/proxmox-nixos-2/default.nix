{ inputs, lib, ... }: {
  imports = [
    inputs.agenix.nixosModules.default
    inputs.disko.nixosModules.default
    ./disko.nix

    ./configuration.nix
    ../../../nixosModules/common
    ../../../nixosModules/server
  ];

  myNixOS.k3s.enable = true;
  myNixOS.tailscale.enable = false;
  # services.k3s.role = lib.mkForce "server";
  # services.k3s.clusterInit = lib.mkForce true;

  networking = { hostName = "proxmox-nixos-2"; };

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  system.stateVersion = "23.11";
}
