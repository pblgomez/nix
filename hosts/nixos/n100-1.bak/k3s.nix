{ config, lib, pkgs, ... }: {
  age.secrets = {
    flux-glab-pat = {
      file = ../../../secrets/flux-glab-pat.age;
      mode = "400";
      owner = "root";
      group = "root";
    };
    k3sToken = {
      file = ../../../secrets/k3sToken.age;
      mode = "400";
      owner = "root";
      group = "root";
    };
  };
  environment = {
    interactiveShellInit = ''
      alias k='${pkgs.kubectl}/bin/kubectl'
      alias kgap='${pkgs.kubectl}/bin/kubectl get pods -A'
      alias kgp='${pkgs.kubectl}/bin/kubectl get pods'
      alias kgn='${pkgs.kubectl}/bin/kubectl get nodes'
      alias k9s='${pkgs.k9s}/bin/k9s'
    '';
    variables = { KUBECONFIG = "/etc/rancher/k3s/k3s.yaml"; };
  };

  services.k3s = {
    enable = true;
    clusterInit = true;
    tokenFile = config.age.secrets.k3sToken.path;
    extraFlags = toString [ "--disable=traefik" ];
    # extraFlags = toString ["--disable=traefik --tls-san=192.168.99.250"];
  };

  systemd.services = {
    setup-flux = {
      description = "Service to bootstrap flux after k3s is started";
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = "yes";
      };
      after = [ "k3s.service" ];
      path = [ pkgs.fluxcd pkgs.kubectl ];
      wantedBy = [ "multi-user.target" ];
      environment = { KUBECONFIG = "/etc/rancher/k3s/k3s.yaml"; };
      script = ''
        export GITLAB_TOKEN="$(cat "${config.age.secrets.flux-glab-pat.path}")"
        flux bootstrap gitlab \
          --components-extra=image-reflector-controller,image-automation-controller \
          --owner=pblgomez --repository=flux --branch=main --path=clusters/n100 \
          --deploy-token-auth --read-write-key --personal
      '';
    };
  };

  networking.firewall.trustedInterfaces = [ "enp1s0" "lo0" "tailscale0" ];
  networking.firewall.enable = lib.mkDefault false;
  networking.firewall.allowedTCPPorts = [ 2379 2380 6443 ];
  networking.firewall.allowedUDPPorts = [
    8472 # k3s, flannel: required if using multi-node for inter-node networking
  ];

  # Longhorn config
  environment.systemPackages = [ pkgs.nfs-utils ];
  services.openiscsi = {
    enable = true;
    name = "${config.networking.hostName}-initiatorhost";
  };
  systemd.tmpfiles.rules =
    [ "L+ /usr/local/bin - - - - /run/current-system/sw/bin/" ];

  # NFS config
  boot.supportedFilesystems = [ "nfs" ];
  services.rpcbind.enable = true;
}
