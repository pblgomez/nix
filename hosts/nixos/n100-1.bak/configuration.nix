{ config, inputs, lib, sshAuthorizedKeys, ... }: {
  imports = [
    inputs.agenix.nixosModules.default
    inputs.disko.nixosModules.disko
    inputs.nixos-hardware.nixosModules.common-gpu-intel-disable
    inputs.nixos-hardware.nixosModules.common-pc-ssd
    inputs.srvos.nixosModules.server
  ];
  boot = {
    loader = {
      timeout = 1;
      efi.canTouchEfiVariables = true;
      systemd-boot.enable = true;
    };
  };
  networking = {
    firewall.allowedTCPPorts = [
      1883 # MQTT
    ];
    hostName = "n100-1";
    useDHCP = lib.mkForce true;
  };

  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
    settings.PermitRootLogin = "yes";
    authorizedKeysFiles =
      lib.mkForce [ "%h/.ssh/authorized_keys /etc/ssh/authorized_keys.d/%u" ];
  };

  systemd.services = {
    sshKeyPublic = {
      description = "TODO: ";
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = "yes";
      };
      after = [ "multi-user.target" ];
      wantedBy = [ "multi-user.target" ];
      script = ''
        source ${config.system.build.setEnvironment}
        chmod 0600 /root/.ssh/id_ed25519
        ssh-keygen -e -f /root/.ssh/id_ed25519 > /root/.ssh/id_ed25519.pub
      '';
    };
  };

  users.users.root.openssh.authorizedKeys.keys = sshAuthorizedKeys;
  system.stateVersion = "23.11";
}
