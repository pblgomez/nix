{
  disko.devices = {
    disk.sda = {
      device = "/dev/disk/by-path/pci-0000:00:17.0-ata-2";
      type = "disk";
      content = {
        type = "gpt";
        partitions = {
          ESP = {
            end = "500M";
            type = "EF00";
            content = {
              type = "filesystem";
              format = "vfat";
              mountpoint = "/boot";
            };
          };
          root = {
            name = "root";
            size = "100%";
            content = {
              type = "filesystem";
              format = "ext4";
              mountpoint = "/";
            };
          };
        };
      };
    };
  };
}
