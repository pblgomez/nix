{
  imports = [
    ../common/default.nix
    ../../common/default.nix
    ../common/tailscale.nix
    ./hardware-configuration.nix
    ./configuration.nix
    ./disko.nix
    ./k3s.nix
  ];
}
