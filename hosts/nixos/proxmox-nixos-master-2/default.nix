{ inputs, lib, ... }: {
  imports = [
    inputs.agenix.nixosModules.default

    inputs.disko.nixosModules.default
    ../common/disko.nix

    ./configuration.nix
    ./hardware-configuration.nix
    ../../../nixosModules/common
    ../../../nixosModules/server
  ];

  myNixOS.k3s.enable = true;
  # services.k3s.clusterInit = lib.mkForce true; # Disable after clusterInit
  services.k3s.role = lib.mkForce "server";
  services.k3s.extraFlags =
    toString [ "--disable=traefik --tls-san=192.168.99.250" ]; # For servers
  myNixOS.tailscale.enable = true;
  services.tailscale = {
    extraUpFlags = [
      "--advertise-exit-node"
      "--ssh"
      "--advertise-routes"
      "192.168.1.0/24,192.168.2.0/24,192.168.3.0/24,192.168.99.0/24"
    ];
    useRoutingFeatures = "server";
  };

  networking = { hostName = "proxmox-nixos-master-2"; };

  system.stateVersion = "24.05";
}
