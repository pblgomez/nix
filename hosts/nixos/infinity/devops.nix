let username = "pbl";
in {
  age.secrets = {
    awsCreds = {
      file = ../../../secrets/awsCreds.age;
      path = "/home/${username}/.config/aws/credentials";
      mode = "400";
      owner = username;
      group = "users";
    };
  };
  # identityPaths = ["/home/${username}/.ssh/id_ed25519" "/etc/ssh/ssh_host_ed25519_key"];
}
