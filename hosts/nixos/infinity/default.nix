{ inputs, ... }:
{

  imports = [
    inputs.agenix.nixosModules.default

    inputs.disko.nixosModules.disko
    ./disko.nix

    inputs.srvos.nixosModules.desktop
    inputs.nixos-hardware.nixosModules.tuxedo-infinitybook-pro14-gen7
    ./configuration.nix
    ./hardware-configuration.nix
    ./kanata.nix
    ../../common
    ./devops.nix
    ../../../nixosModules/common
    ../../../nixosModules/desktop

    inputs.home-manager.nixosModules.home-manager
    {
      home-manager = {
        extraSpecialArgs = { inherit inputs; };
        users."pbl" = {
          imports = [ ../../../home/pbl ];
        };
      };
    }
    inputs.stylix.nixosModules.stylix
  ];

  myNixOS.obsidian.enable = true;
  myNixOS.virtualbox.enable = true;

  system.stateVersion = "24.11";
}
