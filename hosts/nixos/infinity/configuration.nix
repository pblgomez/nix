{
  config,
  inputs,
  lib,
  pkgs,
  sshAuthorizedKeys,
  ...
}:
let
  username = "pbl";
  unstable = import inputs.nixpkgs-unstable { system = pkgs.system; };
in
{
  age = {
    identityPaths = [
      "/home/${username}/.ssh/id_ed25519"
      "/etc/ssh/ssh_host_ed25519_key"
    ];
    secrets = {
      "age" = {
        file = ../../../secrets/age.age;
        path = "/home/${username}/.config/sops/age/keys.txt";
        mode = "400";
        owner = username;
        group = "users";
      };
      "bookmarks" = {
        file = ../../../secrets/bookmarks.age;
        path = "/home/${username}/.local/share/bookmarks";
        mode = "400";
        owner = "${username}";
        group = "users";
      };
      "bwAddress" = {
        file = ../../../secrets/bwAddress.age;
        path = "/home/${username}/.cache/agenix/bwAddress";
        mode = "400";
        owner = username;
        group = "users";
      };
      "openai-api-key" = {
        file = ../../../secrets/openai-api-key.age;
        mode = "400";
        owner = "${username}";
      };
      "sshConfig" = {
        file = ../../../secrets/sshConfig.age;
        path = "/home/${username}/.ssh/config";
        mode = "400";
        owner = username;
        group = "users";
      };
      "sshKey-${username}" = {
        file = ../../../secrets/sshKey-infinity-${username}.age;
        path = "/home/${username}/.ssh/id_ed25519";
        mode = "400";
        owner = username;
        group = "users";
      };
      "email" = {
        file = ../../../secrets/email.age;
        path = "/home/${username}/.cache/agenix/email";
        mode = "400";
        owner = username;
        group = "users";
      };
      "email-veset" = {
        file = ../../../secrets/email-veset.age;
        path = "/home/${username}/.cache/agenix/email-veset";
        mode = "400";
        owner = username;
        group = "users";
      };
    };
  };

  boot = {
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;
    binfmt.emulatedSystems = [
      "aarch64-linux"
      "armv6l-linux"
      "armv7l-linux"
      "riscv64-linux"
    ];
  };

  environment.systemPackages = with pkgs; [
    bitwarden
    brightnessctl
    gparted
    polkit
    localsend
    sshfs
    telegram-desktop
    vault
    wl-clipboard
  ];
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [ "vault" ];
  nixpkgs.config.permittedInsecurePackages = [ "vault-1.16.2" ];
  networking.firewall = {
    trustedInterfaces = [ "tailscale0" ];
    allowedTCPPorts = [ 53317 ]; # Localsend
    allowedUDPPortRanges = [
      {
        from = 53315;
        to = 53318;
      }
      # Localsend
    ];
  };

  hardware = {
    bluetooth.enable = true;
    keyboard.qmk.enable = true;
    ledger.enable = true;
    tuxedo-rs = {
      enable = true;
      tailor-gui.enable = true;
    };
  };

  home-manager = {
    backupFileExtension = "bak";
    users.${username}.stylix.targets = {
      nixvim.enable = false;
      hyprland.enable = false;
      zellij.enable = false;
    };
  };

  # fonts.packages = with pkgs; [ (nerdfonts.override { fonts = [ "FiraCode" "Iosevka" ]; }) ];
  fonts.packages = with pkgs; [ nerd-fonts.iosevka ];

  networking = {
    hostName = "infinity";
    networkmanager.enable = true;
  };

  programs = {
    hyprland.enable = true;
    gnupg.agent = {
      enable = true;
      enableSSHSupport = true;
    };
  };
  security.pam.services.swaylock.text = ''
    # PAM configuration file for the swaylock screen locker. By default, it includes
    # the 'login' configuration file (see /etc/pam.d/login)
    auth include login
  '';

  services = {
    tailscale = {
      useRoutingFeatures = "client";
      enable = true;
      package = unstable.tailscale;
    };
    openssh = {
      enable = true;
    };
  };

  systemd.services = {
    sshKeyPublic = {
      description = "Create Public ssh host key";
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = "yes";
      };
      after = [ "multi-user.target" ];
      wantedBy = [ "multi-user.target" ];
      script = ''
        source ${config.system.build.setEnvironment}
        chmod 0400 /etc/ssh/ssh_host_ed25519_key
        ssh-keygen -e -f /etc/ssh/ssh_host_ed25519_key >/etc/ssh/ssh_host_ed25519_key.pub
      '';
    };
  };

  users.users."root".openssh.authorizedKeys.keys = sshAuthorizedKeys;
  users.users."${username}" = {
    isNormalUser = true;
    initialPassword = username;
    extraGroups = [
      "networkmanager"
      "libvirtd"
      "wheel"
    ];
    openssh.authorizedKeys.keys = sshAuthorizedKeys;
    shell = pkgs.zsh;
  };
  programs.zsh.enable = true;
  virtualisation.podman = {
    enable = true;
    dockerCompat = true;
    autoPrune.enable = true;
  };

  stylix = {
    enable = true;
    base16Scheme = "${pkgs.base16-schemes}/share/themes/gruvbox-dark-hard.yaml";
    fonts = {
      sansSerif = {
        package = pkgs.nerd-fonts.iosevka;
        name = "Iosevka Nerd Font";
      };
      monospace = config.stylix.fonts.sansSerif;
      sizes = {
        applications = 10;
        terminal = 10;
      };
    };
    image = pkgs.fetchurl {
      url = "https://i.imgur.com/5m5kLI9.png";
      sha256 = "sha256-utmDMNeSzboGyup3+0N8hjk8/wgiFctuWYD4u/JptxE=";
    };
    polarity = "dark";
  };

  # Virt-manager
  virtualisation.libvirtd.enable = true;
  programs.virt-manager.enable = true;
}
