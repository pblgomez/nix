{
  disko.devices.disk.main = {
    device = "/dev/disk/by-path/pci-0000:01:00.0-nvme-1";
    type = "disk";
    content = {
      type = "gpt";
      partitions = {
        ESP = {
          size = "500M";
          type = "EF00";
          content = {
            type = "filesystem";
            format = "vfat";
            mountpoint = "/boot";
          };
        };
        systems = {
          size = "100%";
          content = {
            type = "luks";
            name = "systems";
            settings.allowDiscards = true;
            passwordFile = "/tmp/secret.key";
            content = {
              type = "filesystem";
              format = "ext4";
              mountpoint = "/";
            };
          };
        };
      };
    };
  };
}
