{ pkgs, ... }: {
  services.kanata = {
    enable = true;
    package = pkgs.kanata-with-cmd;
    keyboards.infinity.devices =
      [ "/dev/input/by-path/platform-i8042-serio-0-event-kbd" ];
    keyboards.infinity.extraDefCfg = ''
      process-unmapped-keys yes
      linux-unicode-u-code v
    '';
    keyboards.infinity.config = ''
      #|
      (defsrc
        esc       f1   f2   f3  f4 f5 f6   f7   f8   f9   f10  f11  f12  prnt pause ins del
        grv       1    2    3   4  5  6    7    8    9    0    -    =    bspc
        tab       q    w    e   r  t  y    u    i    o    p    [    ]    ret
        caps      a    s    d   f  g  h    j    k    l    ;    '    \
        lsft 102d z    x    c   v  b  n    m    ,    .    /    rsft
        lctl      lmet lalt spc                 ralt rctl pgup up   pgdn
                                                          left down rght
      )
      |#

      (defvar
        tap-timeout    100
        hold-timeout   200
        tt $tap-timeout
        ht $hold-timeout

        mouse-interval 3
        mouse-distance 1
        mouse-distance-minimum 1
        mouse-distance-maximum 4
        mi $mouse-interval
        mdm $mouse-distance-minimum
        mdM $mouse-distance-maximum
      )

      (defsrc
                  q       w     e     r     y     u     i     o     p
        caps      a       s     d     f     h     j     k     l     ;
                  z       x     c     v     n     m     ,     .     /
                          lalt  spc                     ralt
      )

      (deflayer default
                  q       @escw @esce r     y     u     @bspi @bspo p
        esc       @meta   @alts @ctld @sftf h     @sftj @ctlk @altl @met;
                  z       x     c     v     n     m     ,     .     /
                          @numt @spcm                   @navr
      )

      (deflayer nav
                  XX      XX    RA-e  XX    XX    RA-u  RA-i  RA-o  XX
        XX        RA-a    XX    XX    sft   lft   down  up    rght  XX
                  brdown  brup  C-S-c C-S-v RA-n  mute  vold  volu  XX
                          XX    XX                      XX
      )

      (deflayer num
                  `       '     S-9   S-0   \     7     8     9     -
        XX        met     alt   ctl   sft   XX    4     5     6     +
                  S-[     S-]   [     ]     0     1     2     3     =
                          XX    XX                      XX
      )

      (deflayer escw
                  XX      XX    esc   XX    XX    XX    XX    bspc  XX
        XX        XX      XX    XX    XX    XX    XX    XX    XX    XX
                  XX      XX    XX    XX    XX    XX    XX    XX    XX
                          XX    XX                      XX
      )

      (deflayer esce
                  XX      esc   XX    XX    XX    XX    XX    XX    XX
        XX        XX      XX    XX    XX    XX    XX    XX    XX    XX
                  XX      XX    XX    XX    XX    XX    XX    XX    XX
                          XX    XX                      XX
      )

      (deflayer bspi
                  XX      XX    XX    XX    XX    XX    XX    bspc  XX
        XX        XX      XX    XX    XX    XX    XX    XX    XX    XX
                  XX      XX    XX    XX    XX    XX    XX    XX    XX
                          XX    XX                      XX
      )

      (deflayer bspo
                  XX      XX    XX    XX    XX    XX    bspc  XX    XX
        XX        XX      XX    XX    XX    XX    XX    XX    XX    XX
                  XX      XX    XX    XX    XX    XX    XX    XX    XX
                          XX    XX                      XX
      )

      (deflayer mouse
                  XX      XX    XX    XX    @mwl  @mwd  @mwu  @mwr  XX
        XX        XX      XX    XX    XX    @mlf  @mdw  @mup  @mrg  XX
                  XX      XX    XX    XX    XX    mlft  mmid  mrgt  XX
                          XX    XX                      XX
      )

      ;; (deflayer qwerty
      ;;   esc       f1   f2   f3  f4 f5 f6   f7   f8   f9   f10  f11  f12  prnt pause ins del
      ;;   grv       1    2    3   4  5  6    7    8    9    0    -    =    bspc
      ;;   tab       q    w    e   r  t  y    u    i    o    p    [    ]    ret
      ;;   caps      a    s    d   f  g  h    j    k    l    ;    '    \
      ;;   lsft 102d z    x    c   v  b  n    m    ,    .    /    rsft
      ;;   lctl      lmet lalt spc                 ralt rctl pgup up   pgdn
      ;;                                                     left down rght
      ;; )

      (defalias  ;; layers
        spcm (tap-hold $tt $ht spc (layer-while-held mouse))
        navr (tap-hold $tt $ht ret (layer-while-held nav))
        numt (tap-hold $tt $ht tab (layer-while-held num))

        bspi (tap-hold-press 10 $ht i (layer-while-held bspi))
        bspo (tap-hold-press 10 $ht o (layer-while-held bspo))
        escw (tap-hold-press 10 $ht w (layer-while-held escw))
        esce (tap-hold-press 10 $ht e (layer-while-held esce))
      )

      (defalias  ;; home-row mod
        meta (tap-hold $tt $ht a met)
        alts (tap-hold $tt $ht s alt)
        ctld (tap-hold $tt $ht d ctl)
        sftf (tap-hold $tt $ht f sft)
        sftj (tap-hold-release-timeout $tt $ht j sft j)
        ctlk (tap-hold-release-timeout $tt $ht k ctl k)
        altl (tap-hold-release-timeout $tt $ht l alt l)
        met; (tap-hold $tt $ht ; met)
      )

      (defalias  ;; mouse
        mlf (movemouse-accel-left  $mi 1000 $mdm $mdM)
        mdw (movemouse-accel-down  $mi 1000 $mdm $mdM)
        mup (movemouse-accel-up    $mi 1000 $mdm $mdM)
        mrg (movemouse-accel-right $mi 1000 $mdm $mdM)
        mwl (mwheel-left $mi $mdM)
        mwd (mwheel-down $mi $mdM)
        mwu (mwheel-up $mi $mdM)
        mwr (mwheel-right $mi $mdM)
      )
    '';
    keyboards.logitech.devices =
      [ "/dev/input/by-id/usb-Logitech_USB_Receiver-if02-event-kbd" ];
    keyboards.logitech.extraDefCfg = ''
      process-unmapped-keys yes
      linux-unicode-u-code v
    '';
    keyboards.logitech.config = ''
      (defvar
        tap-timeout    100
        hold-timeout   200
        tt $tap-timeout
        ht $hold-timeout
      )
      #|
      (defsrc
        rwd       play   fwd                                                    mute vold volu
        esc       f1     f2    f3    f4    f5 f6   f7    f8    f9    f10   f11  f12  prnt pause ins del
        grv       1      2     3     4     5  6    7     8     9     0     -    =    bspc
        tab       q      w     e     r     t  y    u     i     o     p     [    ]    ret
        caps      a      s     d     f     g  h    j     k     l     ;     '    \
        lsft 102d z      x     c     v     b  n    m     ,     .     /     rsft
        lctl      lmet   lalt  spc                       ralt  rctl  pgup  up   pgdn
                                                                     left  down rght
      )
      |#

      (defsrc
        tab       q      w     e     r     t  y    u     i     o     p     [    ]    ret
        caps      a      s     d     f     g  h    j     k     l     ;     '    \
        lsft 102d z      x     c     v     b  n    m     ,     .     /     rsft
                         lalt  spc                       ralt
      )

      (deflayer default
        tab       q      @escw @esce r     t  y    u     @bspi @bspo p     [    ]    ret
        @ctle     @meta  @alts @ctld @sftf g  h    @sftj @ctlk @altl @met; '    \
        lsft 102d z      x     c     v     b  n    m     ,     .     /     rsft
                         @numt spc                       @navr
      )

      (deflayer bspi
        XX        XX     XX    XX    XX    XX XX   XX    XX    bspc  XX    XX   XX   XX
        XX        XX     XX    XX    XX    XX XX   XX    XX    XX    XX    XX   XX
        XX   XX   XX     XX    XX    XX    XX XX   XX    XX    XX    XX    XX
                         XX    XX                        XX
      )

      (deflayer bspo
        XX        XX     XX    XX    XX    XX XX   XX    bspc  XX    XX    XX   XX   XX
        XX        XX     XX    XX    XX    XX XX   XX    XX    XX    XX    XX   XX
        XX   XX   XX     XX    XX    XX    XX XX   XX    XX    XX    XX    XX
                         XX    XX                        XX
      )

      (deflayer escw
        XX        XX     XX    esc   r     t  y    u     i     o     p     XX   XX   XX
        XX        a      s     d     f     g  h    j     k     l     ;     XX   XX
        XX   XX   XX     XX    XX    XX    XX XX   XX    XX    XX    XX    XX
                         XX    spc                       XX
      )
      (deflayer esce
        _         XX     esc   e     r     t  y    u     i     o     p     XX   XX   XX
        XX        a      s     d     f     g  h    j     k     l     ;     XX   XX
        XX   XX   XX     XX    XX    XX    XX XX   XX    XX    XX    XX    XX
                         XX    spc                       XX
      )

      (deflayer nav
        XX        XX     XX    RA-e  XX    XX XX   RA-u  RA-i  RA-o  XX    XX   XX   XX
        XX        RA-a   alt   ctl   sft   XX lft  down  up    right XX    XX   XX
        XX   XX   brdown brup  C-S-c C-S-v XX RA-n mute  vold  volu  XX    XX
                         XX    0                         XX
      )

      (deflayer num
        XX        `      '     S-9   S-0   XX \    7     8     9     -     XX   XX   XX
        XX        met    alt   ctl   sft   XX XX   4     5     6     +     XX   XX
        XX   XX   S-{    S-}   [     ]     XX XX   1     2     3     =     XX
                         XX    0                         XX
      )

      (defalias  ;; home-row mod
        meta (tap-hold $tt $ht a met)
        alts (tap-hold $tt $ht s alt)
        ctld (tap-hold $tt $ht d ctl)
        sftf (tap-hold $tt $ht f sft)
        sftj (tap-hold-release-timeout $tt $ht j sft j)
        ctlk (tap-hold-release-timeout $tt $ht k ctl k)
        altl (tap-hold-release-timeout $tt $ht l alt l)
        met; (tap-hold $tt $ht ; met)
      )

      (defalias  ;; keys
        ctle (tap-hold $tt $ht esc lctl)
        navr (tap-hold $tt $ht ret (layer-while-held nav))
      )

      (defalias  ;; layers
        bspi (tap-hold-press 10 $ht i (layer-while-held bspi))
        bspo (tap-hold-press 10 $ht o (layer-while-held bspo))

        escw (tap-hold-press 10 $ht w (layer-while-held escw))
        esce (tap-hold-press 10 $ht e (layer-while-held esce))

        numt (tap-hold $tt $ht tab (layer-while-held num))
      )
    '';
  };
}
