{ lib, inputs, ... }: {
  imports = [
    inputs.agenix.nixosModules.default
    inputs.disko.nixosModules.default
    ./disko.nix

    ./configuration.nix
    ../../../nixosModules/common
    ../../../nixosModules/server
  ];

  myNixOS.k3s.enable = true;
  services.k3s.role = lib.mkForce "server";

  networking = { hostName = "vbmaster-2"; };

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  system.stateVersion = "23.11";
}
