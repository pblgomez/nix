{
  config,
  lib,
  sshAuthorizedKeys,
  ...
}:
{

  boot.kernelParams = [
    "cgroup_enable=cpuset"
    "cgroup_enable=memory"
    "cgroup_memory=1"
  ];

  # Disable some modules we don't need
  disabledModules = [
    "profiles/base.nix"
    "profiles/all-hardware.nix"
  ];

  # fileSystems = {
  #   "/mnt/SSD720" = {
  #     device = "/dev/disk/by-label/SSD720";
  #     fsType = "ext4";
  #   };
  # };

  # Hotfix for jmodprobe: FATAL: Module sun4i-drm not found in directory`
  nixpkgs = {
    overlays = [
      (final: super: {
        makeModulesClosure = x: super.makeModulesClosure (x // { allowMissing = true; });
      })
    ];
  };

  users = {
    mutableUsers = false;
    users."root".openssh.authorizedKeys.keys = sshAuthorizedKeys;
  };

  services = {
    openssh = {
      enable = true;
      settings.PasswordAuthentication = false;
      settings.KbdInteractiveAuthentication = false;
      authorizedKeysFiles = lib.mkForce [ "%h/.ssh/authorized_keys /etc/ssh/authorized_keys.d/%u" ];
    };
  };

  # systemd.services = {
  #   create-var-lib-rancher-symlink = {
  #     description = "Create symlink for /var/lib/rancher";
  #     serviceConfig = {
  #       Type = "oneshot";
  #       RemainAfterExit = "yes";
  #     };
  #     after = [ "multi-user.target" ];
  #     wantedBy = [ "multi-user.target" ];
  #     before = [ "dev-disk-by\\x2dlabel-SSD720.device" ];
  #     script = ''
  #       mkdir -p /mnt/SSD720/var/lib/rancher
  #       ln -sfn /mnt/SSD720/var/lib/rancher/ /var/lib/rancher
  #     '';
  #   };
  # };
}
