{
  lib,
  inputs,
  modulesPath,
  ...
}:
{
  imports = [
    (modulesPath + "/installer/sd-card/sd-image-aarch64.nix")
    inputs.nixos-hardware.nixosModules.raspberry-pi-4

    inputs.agenix.nixosModules.default

    ./configuration.nix
    ../../../nixosModules/common
    ../../../nixosModules/server
  ];

  myNixOS.k3s.enable = true;
  myNixOS.tailscale.enable = true;
  # services.tailscale = {
  #   extraUpFlags = [
  #     "--advertise-exit-node"
  #     "--ssh"
  #     "--advertise-routes"
  #     "192.168.1.0/24,192.168.2.0/24,192.168.3.0/24,192.168.99.0/24"
  #   ];
  #   useRoutingFeatures = "server";
  # };

  networking = {
    firewall.enable = lib.mkForce false;
    hostName = "rpi4-2";
  };

  sdImage.compressImage = true;

  nixpkgs.hostPlatform = lib.mkDefault "aarch64-linux";
  system.stateVersion = "24.11";
}
