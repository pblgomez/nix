{ inputs, lib, ... }: {
  imports = [
    inputs.agenix.nixosModules.default
    inputs.disko.nixosModules.disko
    ./disko.nix
    ./hardware-configuration.nix

    ./configuration.nix
    ../../../nixosModules/common
    ../../../nixosModules/server
  ];

  myNixOS.k3s.enable = true;
  services.k3s.role = lib.mkForce "server";
  myNixOS.tailscale.enable = false;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  system.stateVersion = "24.05";
}
