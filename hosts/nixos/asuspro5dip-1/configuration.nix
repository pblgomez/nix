{ config, lib, sshAuthorizedKeys, ... }: {
  boot.loader.grub.enable = true;
  boot.kernelParams = [ "button.lid_init_state=open" ];

  networking.hostName = "asuspro5dip-1";

  services.openssh = {
    enable = true;
    settings.PasswordAuthentication = false;
    settings.KbdInteractiveAuthentication = false;
    settings.PermitRootLogin = "yes";
    authorizedKeysFiles =
      lib.mkForce [ "%h/.ssh/authorized_keys /etc/ssh/authorized_keys.d/%u" ];
  };
  services.logind = {
    extraConfig = "HandlePowerKey=ignore";
    lidSwitch = "ignore";
  };

  users.users."root".openssh.authorizedKeys.keys = sshAuthorizedKeys;
}
