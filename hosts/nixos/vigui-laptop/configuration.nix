{ lib, pkgs, sshAuthorizedKeys, ... }: {
  environment.systemPackages = with pkgs; [
    appimage-run
    libreoffice-qt
    latte-dock
    kdenlive
    gimp
    vlc
  ];

  hardware.bluetooth = {
    enable = true; # enables support for Bluetooth
    powerOnBoot = true; # powers up the default Bluetooth controller on boot
  };

  networking = {
    hostName = "vigui-laptop";
    networkmanager.enable = true;
  };

  services = {
    openssh = {
      enable = true;
      settings.PermitRootLogin = "yes";
      authorizedKeysFiles =
        lib.mkForce [ "%h/.ssh/authorized_keys /etc/ssh/authorized_keys.d/%u" ];
    };
    tailscale.enable = true;
  };

  users.users = {
    "root".openssh.authorizedKeys.keys = sshAuthorizedKeys;
    "vigui" = {
      isNormalUser = true;
      initialPassword = "vigui";
      extraGroups = [ "networkmanager" "wheel" ];
      openssh.authorizedKeys.keys = sshAuthorizedKeys;
    };
  };

  services = {
    displayManager.sddm.enable = true;
    displayManager.sddm.wayland.enable = true;
    displayManager.defaultSession = "plasmawayland";
    xserver = {
      enable = true;
      desktopManager.plasma5.enable = true;
    };
  };
}
