{ inputs, ... }: {
  imports = [
    inputs.agenix.nixosModules.default

    inputs.disko.nixosModules.disko
    ./disko.nix

    inputs.srvos.nixosModules.desktop
    ./configuration.nix
    ./hardware-configuration.nix
    ../../../nixosModules/desktop

    inputs.home-manager.nixosModules.home-manager
    {
      home-manager = {
        extraSpecialArgs = { inherit inputs; };
        users."vigui" = { imports = [ ../../../home/vigui ]; };
      };
    }
  ];

  myNixOS.steam.enable = true;

  system.stateVersion = "23.11";
}
