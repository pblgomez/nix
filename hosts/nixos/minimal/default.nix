{ inputs, lib, sshAuthorizedKeys, ... }: {
  imports = [ inputs.disko.nixosModules.disko ../common/disko.nix ];

  users = { users."root".openssh.authorizedKeys.keys = sshAuthorizedKeys; };
  users.users."root" = { initialPassword = "toor"; }; # TODO: Remove

  services = {
    openssh = {
      enable = true;
      authorizedKeysFiles =
        lib.mkForce [ "%h/.ssh/authorized_keys /etc/ssh/authorized_keys.d/%u" ];
    };
  };

  networking.hostName = "minimal";
  nixpkgs.hostPlatform = "x86_64-linux";
  system.stateVersion = "24.05";
}
