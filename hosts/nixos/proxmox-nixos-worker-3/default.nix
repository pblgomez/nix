{ inputs, lib, ... }: {
  imports = [
    inputs.agenix.nixosModules.default

    inputs.disko.nixosModules.default
    ./disko.nix

    ./configuration.nix
    ./hardware-configuration.nix

    ../../../nixosModules/common
    ../../../nixosModules/server
  ];

  myNixOS.k3s.enable = true;
  myNixOS.tailscale.enable = false;

  networking = { hostName = "proxmox-nixos-worker-3"; };

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  system.stateVersion = "24.05";
}
