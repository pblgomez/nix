{ inputs, ... }: {
  imports = [
    inputs.agenix.nixosModules.default
    inputs.disko.nixosModules.disko
    ./configuration.nix
    ./hardware-configuration.nix
    ./disko.nix
  ];
}
