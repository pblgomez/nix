{ config, lib, pkgs, sshAuthorizedKeys, ... }: {
  age.secrets = {
    k3sToken = {
      file = ../../../secrets/k3sToken.age;
      mode = "400";
      owner = "root";
      group = "root";
    };
  };
  environment = {
    interactiveShellInit = ''
      alias k='${pkgs.kubectl}/bin/kubectl'
      alias kgap='${pkgs.kubectl}/bin/kubectl get pods -A'
      alias kgp='${pkgs.kubectl}/bin/kubectl get pods'
      alias kgn='${pkgs.kubectl}/bin/kubectl get nodes'
      alias k9s='${pkgs.k9s}/bin/k9s'
    '';
    variables = { KUBECONFIG = "/etc/rancher/k3s/k3s.yaml"; };
  };

  networking = {
    hostName = "libvirt";
    firewall.trustedInterfaces = [ "enp0s3" "enp1s0" "lo0" "tailscale0" ];
    firewall.enable = lib.mkDefault false;
  };

  services = {
    k3s = {
      enable = true;
      role = "agent";
      tokenFile = config.age.secrets.k3sToken.path;
      serverAddr = "https://192.168.99.250:6443";
    };
    openssh = {
      enable = true;
      settings.PasswordAuthentication = false;
      settings.KbdInteractiveAuthentication = false;
      settings.PermitRootLogin = "yes";
    };
  };

  # Longhorn config
  environment.systemPackages = [ pkgs.nfs-utils ];
  services.openiscsi = {
    enable = true;
    name = "${config.networking.hostName}-initiatorhost";
  };
  systemd.tmpfiles.rules =
    [ "L+ /usr/local/bin - - - - /run/current-system/sw/bin/" ];

  users.users."root".openssh.authorizedKeys.keys = sshAuthorizedKeys;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  system.stateVersion = "23.11";
}
