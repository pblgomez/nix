{ lib, inputs, ... }: {
  imports = [
    inputs.agenix.nixosModules.default
    ./configuration.nix
    ../../../nixosModules/common

    inputs.home-manager.nixosModules.home-manager
    {
      home-manager = {
        extraSpecialArgs = { inherit inputs; };
        users."pbl" = { imports = [ ../../../home/pbl ]; };
      };
    }
  ];

  networking = {
    firewall.enable = lib.mkDefault false;
    hostName = "utm-work";
  };

  nixpkgs.hostPlatform = lib.mkDefault "aarch64-linux";
  system.stateVersion = "23.11";
}
