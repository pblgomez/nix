{ config, lib, pkgs, sshAuthorizedKeys, modulesPath, ... }:
let username = "pbl";
in {
  imports = [ (modulesPath + "/profiles/qemu-guest.nix") ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.initrd.availableKernelModules =
    [ "xhci_pci" "virtio_pci" "usbhid" "usb_storage" "sr_mod" ];
  fileSystems."/" = {
    device = "/dev/disk/by-uuid/63311cf0-5855-4957-bc7e-40aa078f6160";
    fsType = "ext4";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/2D49-BB23";
    fsType = "vfat";
    options = [ "fmask=0022" "dmask=0022" ];
  };
  networking.useDHCP = lib.mkDefault true;
  nixpkgs.hostPlatform = lib.mkDefault "aarch64-linux";
  services.openssh.enable = true;
  services.qemuGuest.enable = true;
  services.xserver.enable = true;
  services.xserver.displayManager.lightdm.enable = true;
  services.xserver.desktopManager.lxqt.enable = true;
  users.users = {
    "root".openssh.authorizedKeys.keys = sshAuthorizedKeys;
    "${username}" = {
      isNormalUser = true;
      initialPassword = "${username}";
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = sshAuthorizedKeys;
      shell = pkgs.zsh;
    };
  };
  programs.zsh.enable = true;
}
