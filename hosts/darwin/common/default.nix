{ sshAuthorizedKeys, ... }:
let
  username = "pbl";
in
{
  imports = [ ./dock.nix ];
  nix = {
    gc = {
      automatic = true;
      interval = {
        Weekday = 0;
        Hour = 0;
        Minute = 0;
      };
    };
    settings.experimental-features = "flakes nix-command";
  };
  local = {
    dock.enable = true;
    dock.entries = [
      { path = "/Applications/Firefox.app"; }
      { path = "/Applications/Ghostty.app"; }
      { path = "/Applications/Nix Apps/Slack.app"; }
    ];
  };
  programs.zsh.enable = true;
  security.pam.enableSudoTouchIdAuth = true;
  services.nix-daemon.enable = true;
  system = {
    defaults = {
      controlcenter.BatteryShowPercentage = true;
      controlcenter.Sound = true;
      dock = {
        autohide = true;
        mru-spaces = false; # Don't auto rearrange spaces
        orientation = "left";
      };
      hitoolbox.AppleFnUsageType = "Do Nothing";
      finder = {
        AppleShowAllExtensions = true;
        FXPreferredViewStyle = "icnv";
        FXRemoveOldTrashItems = true;
        ShowPathbar = true;
      };
      NSGlobalDomain = {
        AppleInterfaceStyle = "Dark";
        _HIHideMenuBar = false;
        "com.apple.mouse.tapBehavior" = 1;
      };
      screencapture.location = "˜/Pictures/screenshots";
      SoftwareUpdate.AutomaticallyInstallMacOSUpdates = true;
    };
    keyboard = {
      enableKeyMapping = true;
      remapCapsLockToEscape = true;
      swapLeftCtrlAndFn = true;
    };
    stateVersion = 5;
  };
  users.users.${username} = {
    home = /Users/${username};
    openssh.authorizedKeys.keys = sshAuthorizedKeys;
  };
}
