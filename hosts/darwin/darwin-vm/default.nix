{
  inputs,
  lib,
  ...
}: {
  imports = [
    ../common
    ../../common
    inputs.home-manager.darwinModules.home-manager
    {
      home-manager = {
        extraSpecialArgs = {inherit inputs;};
        users."pbl" = {
          imports = [../../../home/darwin.nix];
        };
      };
    }
  ];

  nixpkgs.hostPlatform = lib.mkDefault "aarch64-darwin";
}
