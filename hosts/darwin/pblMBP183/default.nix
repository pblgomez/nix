{
  inputs,
  lib,
  pkgs,
  ...
}:
let
  username = "pbl";
  hostname = "pblMBP183";
in
{
  imports = [
    ../common/default.nix
    inputs.agenix.darwinModules.default
    inputs.home-manager.darwinModules.home-manager
    {
      home-manager = {
        useGlobalPkgs = true;
        useUserPackages = true;
        extraSpecialArgs = { inherit inputs; };
        users.${username} = {
          imports = [
            ../../../home/pbl/starship.nix
            ../../../home/pbl/zsh.nix
            ../../../home/pbl/git
            ../../../home/common
            {
              home.stateVersion = "24.11";
              home.file.".config/skhd/skhdrc" = {
                text = ''
                  cmd + alt - v: "/Applications/Nix Apps/Slack.app/Contents/MacOS/Slack"
                  cmd + alt - t: open -n "/Applications/Ghostty.app"
                  cmd + alt - b : open -n "/Applications/Firefox.app"
                '';
              };
              programs.aerospace = {
                enable = true;
                userSettings = {
                  automatically-unhide-macos-hidden-apps = true;
                  mode.main.binding = {
                    alt-h = "focus left";
                    alt-j = "focus right";
                    alt-k = "focus up";
                    alt-l = "focus right";
                    cmd-1 = "workspace 1";
                    cmd-2 = "workspace 2";
                    cmd-3 = "workspace 3";
                    cmd-4 = "workspace 4";
                    cmd-5 = "workspace 5";
                    cmd-6 = "workspace 6";
                    cmd-7 = "workspace 7";
                    cmd-8 = "workspace 8";
                    cmd-9 = "workspace 9";
                    cmd-shift-1 = "move-node-to-workspace 1";
                    cmd-shift-2 = "move-node-to-workspace 2";
                    cmd-shift-3 = "move-node-to-workspace 3";
                    cmd-shift-4 = "move-node-to-workspace 4";
                    cmd-shift-5 = "move-node-to-workspace 5";
                    cmd-shift-6 = "move-node-to-workspace 6";
                    cmd-shift-7 = "move-node-to-workspace 7";
                    cmd-shift-8 = "move-node-to-workspace 8";
                    cmd-shift-9 = "move-node-to-workspace 9";
                  };
                };
                userSettings = {
                  start-at-login = true;
                };
              };
            }
          ];
        };
      };
    }
    inputs.nix-homebrew.darwinModules.nix-homebrew
    {
      nix-homebrew = {
        enable = true;
        user = "${username}";
      };
    }
  ];
  age = {
    secrets = {
      awsCreds = {
        file = ../../../secrets/awsCreds.age;
        path =
          if pkgs.stdenv.isDarwin then
            "/Users/${username}/.config/aws/credentials"
          else
            "/home/${username}/.config/aws/credentials";
        mode = "400";
        owner = username;
        group = if pkgs.stdenv.isDarwin then "staff" else "users";
      };
      openai-api-key = {
        file = ../../../secrets/openai-api-key.age;
        mode = "400";
        owner = username;
      };
    };
  };
  nixpkgs.config.allowUnfreePredicate = pkg: builtins.elem (lib.getName pkg) [ "slack" ];
  environment.systemPackages = with pkgs; [ slack ] ++ [ inputs.nvf.packages.${system}.neovim ];
  homebrew = {
    enable = true;
    casks = [
      "bitwarden"
      "nikitabobko/tap/aerospace"
      "firefox"
      "ghostty"
      "tailscale"
    ];
    masApps = {
    };
  };
  networking.computerName = hostname;
  networking.hostName = hostname;
  nixpkgs.hostPlatform = "aarch64-darwin";
  services = {
    skhd.enable = true;
  };
}
