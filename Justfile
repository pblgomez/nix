# Build the system config and switch to it when running `just` with no args
default: switch

hostname := `hostname | cut -d "." -f 1`

### macos
# Build the nix-darwin system configuration without switching to it
[macos]
build target_host=hostname flags="":
  @echo "Building nix-darwin config..."
  nix --extra-experimental-features 'nix-command flakes'  build ".#darwinConfigurations.{{target_host}}.system" {{flags}} --impure

# Build the nix-darwin config with the --show-trace flag set
[macos]
trace target_host=hostname: (build target_host "--show-trace")

# Build the nix-darwin configuration and switch to it
[macos]
switch target_host=hostname: (build target_host)
  @echo "switching to new config for {{target_host}}"
  ./result/sw/bin/darwin-rebuild switch --flake ".#{{target_host}}" --impure

### linux
# Build the NixOS configuration without switching to it
[linux]
build target_host=hostname flags="":
  nixos-rebuild build --flake .#{{target_host}} {{flags}}

# Build the NixOS config with the --show-trace flag set
[linux]
trace target_host=hostname: (build target_host "--show-trace")

# Build the NixOS configuration and switch to it.
[linux]
switch target_host=hostname:
  sudo nixos-rebuild switch --flake .#{{target_host}}

[linux]
anywhere name target scrtsdir="/tmp/scrts/":
  printf "Building %s to deploy in %s\n" "{{name}}" "{{target}}"
  mkdir -p {{scrtsdir}}etc/ssh/
  # nix run github:ryantm/agenix -- -d secrets/sshKey-{{name}}.age > {{scrtsdir}}root/.ssh/id_ed25519 && chmod 600 {{scrtsdir}}root/.ssh/id_ed25519
  nix run github:ryantm/agenix -- -d secrets/tailscaleToken.age > {{scrtsdir}}tailscaleToken
  nix run github:ryantm/agenix -- -d secrets/sshHostKey-{{name}}.age > {{scrtsdir}}etc/ssh/ssh_host_ed25519_key
  nix run github:ryantm/agenix -- -d secrets/sshHostKeyPublic-{{name}}.age > {{scrtsdir}}etc/ssh/ssh_host_ed25519_key.pub
  nix run github:nix-community/nixos-anywhere -- --flake .#{{name}} --option pure-eval false --extra-files "{{scrtsdir}}" "{{target}}"
  # nix run github:nix-community/nixos-anywhere -- --flake .#{{name}} --option pure-eval false "{{target}}" --vm-test
  rm -rfv {{scrtsdir}}

[linux]
remote-switch name target scrtsdir="/tmp/scrts/":
  mkdir -p {{scrtsdir}}/root/.ssh
  # nix run github:ryantm/agenix -- -d secrets/sshKey-{{name}}.age > {{scrtsdir}}root/.ssh/id_ed25519
  nix run github:ryantm/agenix -- -d secrets/tailscaleToken.age > {{scrtsdir}}tailscaleToken
  nixos-rebuild switch --flake .#{{name}} --target-host {{target}} --build-host {{target}} --impure
  rm -rfv {{scrtsdir}}

# Update flake inputs to their latest revisions
update:
  nix flake update


# Garbage collect old OS generations and remove stale packages from the nix store
gc generations="5d":
  nix-env --delete-generations {{generations}}
  nix-store --gc
