# Nix config

## Linux

```bash
# To enter the devShell
nix develop --extra-experimental-features 'flakes nix-command'

# Change hostname and Install with
just
```

## MacOS

```bash
curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install

# Install Xcode tools

xcode-select --install

nix run nix-darwin#darwin-rebuild switch -- --flake .#
```

## Tests

### Local vm

```bash
nixos-rebuild build-vm --flake .#linux-vm
```

### AWS

```bash
NIX_SSHOPTS="-i ~/Downloads/pbl-dev3.pem" nixos-rebuild switch --flake .#nix-cloud-test --target-host root@ec2-35-152-48-138.eu-south-1.compute.amazonaws.com
```
