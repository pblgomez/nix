{
  description = "Another try at improving my NixOS Configuration";
  inputs = {
    # nixpkgs.url = "nixpkgs/nixos-24.11";
    nixpkgs.url = "nixpkgs/nixpkgs-unstable";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    # nixpkgs-darwin.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    nixpkgs-darwin.url = "github:NixOS/nixpkgs/nixpkgs-24.11-darwin";
    nix-darwin = {
      url = "github:lnl7/nix-darwin/nix-darwin-24.11";
      # url = "github:lnl7/nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs-darwin";
    };
    nix-homebrew.url = "github:zhaofengli-wip/nix-homebrew";
    homebrew-core = {
      url = "github:homebrew/homebrew-core";
      flake = false;
    };
    homebrew-cask = {
      url = "github:homebrew/homebrew-cask";
      flake = false;
    };

    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    firefox-addons = {
      url = "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      # url = "github:nix-community/home-manager/release-24.11";
      url = "github:nix-community/home-manager";
    };
    nixos-hardware.url = "github:NixOS/nixos-hardware/master";
    nvf.url = "github:pblgomez/nvf";
    srvos = {
      url = "github:numtide/srvos";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    stylix.url = "github:danth/stylix/release-24.11";
  };
  nixConfig = {
    experimental-features = [
      "nix-command"
      "flakes"
    ];
    extra-substituters = [
      "https://hyprland.cachix.org"
      "https://cache.nixos.org"
      "https://devenv.cachix.org"
    ];
    extra-trusted-public-keys = [
      "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
      "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
      "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw=;"
    ];
  };
  outputs =
    {
      nixpkgs,
      nix-darwin,
      nix-homebrew,
      homebrew-core,
      homebrew-cask,
      ...
    }@inputs:
    let
      systems = [
        "x86_64-linux"
        "aarch64-linux"
        "aarch64-darwin"
      ];
      sshAuthorizedKeys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEoe8CHQx53FwXs7SxKoYb8qgivXEza6vdIobOOwsYtP pbl@infinity"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHNSSAHSIoF0YSuBDaW0V+8U2VIyqC5QY5WnTb4EmTOY nix-on-droid@pixel6"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPxvcE3SNJQLy6DkQpRhOCviOh9d9G9Cy0GUsMlo7bh1 pbl@pbl-mbp183"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFPwyUa5MPvY/9Q36hrSqosB25VOFuHcgOi4gQSpO0l1 connectbot@pixel6"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG6CUCfrQGYHpT6Sg0zSTAM/Wl0rUrBJkGvO2fomIdvA pbl@pblMBP183"
      ];
      forEachSystem =
        f:
        nixpkgs.lib.genAttrs systems (
          system:
          f (
            import nixpkgs {
              inherit system;
              nixpkgs.config.allowUnfree = true;
            }
          )
        );
      nixosSystemFor =
        hostname:
        nixpkgs.lib.nixosSystem {
          specialArgs = { inherit inputs sshAuthorizedKeys; };
          modules = [ ./hosts/nixos/${hostname} ];
        };
      darwinSystemFor =
        hostname:
        nix-darwin.lib.darwinSystem {
          specialArgs = { inherit inputs sshAuthorizedKeys; };
          modules = [ ./hosts/darwin/${hostname} ];
        };
    in
    rec {
      nixosConfigurations = {
        "infinity" = nixosSystemFor "infinity";
        "vigui-laptop" = nixosSystemFor "vigui-laptop";
        "n100-1" = nixosSystemFor "n100-1";
        "asuspro5dip-1" = nixosSystemFor "asuspro5dip-1";

        "linux-vm" = nixosSystemFor "linux-vm";
        "n100-1-desktop" = nixosSystemFor "n100-1-desktop";
        "rpi4-2" = nixosSystemFor "rpi4-2";
        "libvirt" = nixosSystemFor "libvirt";
        "vbmaster-1" = nixosSystemFor "vbmaster-1";
        "vbmaster-2" = nixosSystemFor "vbmaster-2";
        "utm-work" = nixosSystemFor "utm-work";
        "ec2-nixos-pbl" = nixosSystemFor "ec2-nixos-pbl";

        "proxmox-nixos-worker-1" = nixosSystemFor "proxmox-nixos-worker-1";
        "proxmox-nixos-worker-2" = nixosSystemFor "proxmox-nixos-worker-2";
        "proxmox-nixos-worker-3" = nixosSystemFor "proxmox-nixos-worker-3";
        "proxmox-nixos-2" = nixosSystemFor "proxmox-nixos-2";
        "proxmox-nixos-3" = nixosSystemFor "proxmox-nixos-3";

        "proxmox-nixos-master-1" = nixosSystemFor "proxmox-nixos-master-1";
        "proxmox-nixos-master-2" = nixosSystemFor "proxmox-nixos-master-2";
        "proxmox-nixos-master-3" = nixosSystemFor "proxmox-nixos-master-3";
        "proxmox-nixos-master-4" = nixosSystemFor "proxmox-nixos-master-4";

        "minimal" = nixosSystemFor "minimal";
      };
      darwinConfigurations = {
        "darwin-vm" = darwinSystemFor "darwin-vm";
        "pblMBP183" = darwinSystemFor "pblMBP183";
      };
      image = {
        rpi4-2 = nixosConfigurations.rpi4-2.config.system.build.sdImage;
      };
      devShells = forEachSystem (pkgs: {
        default = pkgs.mkShell {
          packages = with pkgs; [
            fluxcd
            git
            nixos-rebuild
            just
            go-task
          ];
        };
      });
      formatter = forEachSystem (pkgs: pkgs.nixfmt-rfc-style);
    };
}
