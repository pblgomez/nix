{
  config,
  lib,
  ...
}: {
  options.myNixOS.nix.enable =
    lib.mkEnableOption "enable Garbage collector";

  config = lib.mkIf config.myNixOS.nix.enable {
    nix = {
      gc.automatic = true;
      settings.auto-optimise-store = true;
      settings.experimental-features = ["nix-command" "flakes"];
    };
  };
}
