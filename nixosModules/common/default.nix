{ lib, ... }: {
  imports = [ ./nix.nix ];
  myNixOS.nix.enable = lib.mkDefault true;
  time.timeZone = lib.mkForce "Atlantic/Canary";
}
