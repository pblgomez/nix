{
  config,
  lib,
  pkgs,
  ...
}:
{
  options.myNixOS.k3s.enable = lib.mkEnableOption "enable k3s agent";

  config = lib.mkIf config.myNixOS.k3s.enable {
    age = {
      secrets = {
        k3sToken = {
          file = ../../secrets/k3sToken.age;
          mode = "400";
          owner = "root";
          group = "root";
        };
      };
    };

    networking = {
      firewall.enable = false;
    };

    services.k3s = {
      enable = true;
      role = "agent";
      tokenFile = config.age.secrets.k3sToken.path;
      # extraFlags = toString [ "--disable=traefik --tls-san=192.168.99.250" ]; # For servers

      # Cluster Init
      # clusterInit = lib.mkForce true; # Disable after clusterInit
      serverAddr = "https://192.168.99.250:6443"; # Disable on clusterInit
    };
    # Longhorn config
    environment.systemPackages = [ pkgs.nfs-utils ];
    services.openiscsi = {
      enable = true;
      name = "${config.networking.hostName}-initiatorhost";
    };
    systemd.tmpfiles.rules = [ "L+ /usr/local/bin - - - - /run/current-system/sw/bin/" ];
  };
}
