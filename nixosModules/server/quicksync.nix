# Test hardware acc with: /usr/lib/jellyfin-ffmpeg/ffmpeg -v verbose -init_hw_device vaapi=va:/dev/dri/renderD128 -init_hw_device opencl@va
{ config, inputs, lib, pkgs, ... }:
let
  unstable = import inputs.nixpkgs-unstable {
    config.allowUnfree = true;
    system = pkgs.system;
  };
in {
  options.myNixOS.quicksync.enable = lib.mkEnableOption "enable quicksync";

  config = lib.mkIf config.myNixOS.quicksync.enable {
    boot.kernelParams = [ "intel_iommu=on" ];
    boot.kernelPackages = pkgs.linuxPackages_latest;
    environment.sessionVariables = {
      NEOReadDebugKeys = "1";
      OverrideGpuAddressSpace = "48";
      LIBVA_DRIVER_NAME = "iHD";
    }; # Force intel-media-driver
    hardware.opengl = {
      enable = true;
      extraPackages = with unstable; [
        intel-media-driver
        intel-media-sdk
        intel-compute-runtime
        intel-vaapi-driver
        vaapiVdpau
        vaapiIntel
        libvdpau-va-gl
        onevpl-intel-gpu
        # intel-ocl
      ];
    };
  };
}
