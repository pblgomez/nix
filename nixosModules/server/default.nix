{ lib, ... }: {
  imports = [ ./k3s.nix ./quicksync.nix ./tailscale.nix ];
  myNixOS.tailscale.enable = lib.mkDefault true;
}
