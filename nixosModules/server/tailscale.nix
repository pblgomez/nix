{ config, lib, ... }: {
  options.myNixOS.tailscale.enable = lib.mkEnableOption "enable tailscale";

  config = lib.mkIf config.myNixOS.tailscale.enable {
    age.secrets = {
      "tailscaleToken" = {
        file = ../../secrets/tailscaleToken.age;
        mode = "400";
        group = "users";
      };
    };
    services.tailscale = {
      enable = true;
      useRoutingFeatures = "server";
      authKeyFile = config.age.secrets.tailscaleToken.path;
    };
  };
}
