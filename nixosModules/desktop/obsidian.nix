{
  config,
  inputs,
  lib,
  pkgs,
  ...
}: let
  unstable = import inputs.nixpkgs-unstable {
    system = pkgs.system;
    config.allowUnfree = true;
  };
in {
  options.myNixOS.obsidian.enable =
    lib.mkEnableOption "enable Obsidian";

  config = lib.mkIf config.myNixOS.obsidian.enable {
    environment.systemPackages = [
      unstable.obsidian
    ];
  };
}
