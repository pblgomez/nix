{ config, inputs, lib, pkgs, ... }:
let
  unstable = import inputs.nixpkgs-unstable {
    system = pkgs.system;
    config = {
      allowUnfree = true;
      allowUnfreePredicate = pkg:
        builtins.elem (lib.getName pkg) [
          "steam"
          "steam-original"
          "steam-run"
        ];
    };
  };
in {
  options.myNixOS.steam.enable = lib.mkEnableOption "enable Steam";

  config = lib.mkIf config.myNixOS.steam.enable {
    environment.systemPackages = with unstable; [
      bottles
      mangohud
      protonup
      lutris
      wine
    ];
    programs.steam.enable = true;
    programs.steam.gamescopeSession.enable = true;
    programs.gamemode.enable = true;
  };
}
