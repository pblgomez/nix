{lib, ...}: {
  imports = [
    ./obsidian.nix
    ./spotify.nix
    ./virtualbox.nix
    ./steam.nix
  ];
  myNixOS.spotify.enable = lib.mkDefault true;
}
