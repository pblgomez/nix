{
  config,
  lib,
  ...
}: {
  options.myNixOS.virtualbox.enable =
    lib.mkEnableOption "enable Virtualbox";

  config = lib.mkIf config.myNixOS.virtualbox.enable {
    # virtualisation.virtualbox.host.enable = true;
    users.extraGroups.vboxusers.members = ["pbl"]; # TODO: inherit from somewhere...
  };
}
