{ config, lib, pkgs, ... }: {
  options.myNixOS.spotify.enable = lib.mkEnableOption "enable Spotify";

  config = lib.mkIf config.myNixOS.spotify.enable {
    environment.systemPackages = with pkgs; [ spotify ];
    nixpkgs.config.allowUnfree = true;
  };
}
