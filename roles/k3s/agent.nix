{
  config,
  inputs,
  pkgs,
  ...
}: {
  imports = [
    inputs.agenix.nixosModules.default
  ];

  age.secrets = {
    k3sToken = {
      file = ../../secrets/k3sToken.age;
      mode = "400";
      owner = "root";
      group = "root";
    };
  };

  networking = {
    firewall.enable = false;
  };

  services.k3s = {
    enable = true;
    role = "agent";
    tokenFile = config.age.secrets.k3sToken.path;
    serverAddr = "https://192.168.99.250:6443";
  };

  # Longhorn config
  environment.systemPackages = [pkgs.nfs-utils];
  services.openiscsi = {
    enable = true;
    name = "${config.networking.hostName}-initiatorhost";
  };
  systemd.tmpfiles.rules = [
    "L+ /usr/local/bin - - - - /run/current-system/sw/bin/"
  ];
}
