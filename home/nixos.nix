{inputs, ...}: {
  imports = [
    inputs.nixvim.homeManagerModules.nixvim
    ./apps/devops
    # ./apps/common
  ];

  home.stateVersion = "23.11";
}
