{
  programs.git = {
    enable = true;
    userName = "Pablo Gómez";
    userEmail = "pablogomez@pablogomez.com";
    signing = { key = "E33576E61E060DAD"; };
    aliases = {
      aliases =
        "!git config --list | grep 'alias\\.' | sed 's/alias\\.\\([^=]*\\)=\\(.*\\)/\\1\\ 	 => \\2/' | sort";
      a = "add";
      b = "branch";
      c = "commit";
      ci = "commit";
      co = "checkout";
      cob = "checkout -b";
      l =
        "log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit";
      pl = "pull";
      s = "status --short";
      staash = "stash --all";
    };
    extraConfig = {
      init = { defaultBranch = "main"; };
      core = { excludefile = "$HOME/.config/git/gitignore"; };
      push = { autoSetupRemote = true; };
      rerere.enable = true;
    };
    includes = [{
      condition = "gitdir:~/.cache/gits/veset/";
      path = "~/.cache/gits/veset/.gitconfig";
    }];
  };
  home.file.".cache/gits/veset/.gitconfig" = {
    text = ''
      [user]
        name = Pablo Gómez
        email = "pablo.gomez@veset.tv"
    '';
  };
}
