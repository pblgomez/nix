{ inputs, pkgs, ... }:
let
  unstable = import inputs.nixpkgs-unstable {
    inherit (pkgs) system; # system = pkgs.system;
    # Uncomment this if you need an unfree package from unstable.
    #config.allowUnfree = true;
  };
in
{
  home = {
    packages = [
      pkgs.wtype
      unstable.rofi-rbw
      pkgs.wofi
    ];
    activation.rbw-pbl = ''
      rm -rfv ~/.config/rbw-pbl/config.json
      mkdir -p ~/.config/rbw-pbl
      cp ~/.config/rbw/config.json ~/.config/rbw-pbl/config.json
      ${pkgs.sd}/bin/sd '<email>' "$(cat ~/.cache/agenix/email)" ~/.config/rbw-pbl/config.json
      ${pkgs.sd}/bin/sd '"base_url": null' "\"base_url\": \"$(cat ~/.cache/agenix/bwAddress)\"" ~/.config/rbw-pbl/config.json
    '';
    activation.rbw-veset = ''
      rm -rfv ~/.config/rbw-veset/config.json
      mkdir -p ~/.config/rbw-veset
      cp ~/.config/rbw/config.json ~/.config/rbw-veset/config.json
      ${pkgs.sd}/bin/sd '<email>' "$(cat ~/.cache/agenix/email-veset)" ~/.config/rbw-veset/config.json
      ${pkgs.sd}/bin/sd '"identity_url".*' '"identity_url": "/identity",' ~/.config/rbw-veset/config.json
      ${pkgs.sd}/bin/sd '"base_url": null' "\"base_url\": \"https://api.bitwarden.com/\"" ~/.config/rbw-veset/config.json
    '';
  };
  programs.rbw = {
    enable = true;
    package = unstable.rbw;
    # Use for template only
    settings = {
      email = "<email>";
      pinentry = pkgs.pinentry-qt;
    };
  };
}
