{ inputs, pkgs, ... }:

let
  unstable = import inputs.nixpkgs-unstable { inherit (pkgs) system; };
in
{
  imports = [
    ./git
    ./kanshi.nix
    ./hyprland.nix
    ./firefox.nix
    ./starship.nix
    ../common/default.nix
    ./zsh.nix
    ./zellij.nix
    ./devops
    ./rbw.nix
  ];

  nixpkgs.config.allowUnfree = true;

  home = {
    packages =
      with pkgs;
      [
        devenv
        jq
        ripgrep
        joplin
        tldr
        unzip
      ]
      ++ [ inputs.nvf.packages.${system}.neovim ];
    stateVersion = "24.11";
  };

  programs = {
    bat.enable = true;
    eza = {
      enable = true;
      git = true;
      icons = "auto";
    };
    fzf = {
      enable = true;
      enableZshIntegration = true;
    };
    git = {
      enable = true;
      userName = "Pablo Gómez";
      userEmail = "pablogomez@pablogomez.com";
      signing = {
        key = "E33576E61E060DAD";
      };
      aliases = {
        aliases = "!git config --list | grep 'alias\\.' | sed 's/alias\\.\\([^=]*\\)=\\(.*\\)/\\1\\ 	 => \\2/' | sort";
        a = "add";
        b = "branch";
        c = "commit";
        ci = "commit";
        co = "checkout";
        cob = "checkout -b";
        l = "log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit";
        pl = "pull";
        s = "status --short";
      };
      extraConfig = {
        init = {
          defaultBranch = "main";
        };
        core = {
          excludefile = "$HOME/.config/git/gitignore";
        };
        push = {
          autoSetupRemote = true;
        };
      };
      includes = [
        {
          condition = "gitdir:~/.cache/gits/veset/";
          path = "~/.cache/gits/veset/.gitconfig";
        }
      ];
    };
    home-manager.enable = true;
    ghostty = {
      enable = true;
      enableZshIntegration = true;
      installVimSyntax = true;
      settings = {
        background-opacity = 0.9;
        font-size = 10;
        window-decoration = false;
      };
    };
    kitty = {
      enable = true;
      environment = {
        TERM = "xterm-256color";
      };
      shellIntegration.enableZshIntegration = true;
      settings.confirm_os_window_close = 0;
    };
    lf = {
      enable = true;
      commands = {
        editor-open = "$$EDITOR $f";
      };
      extraConfig =
        let
          previewer = pkgs.writeShellScriptBin "pv.sh" ''
            file=$1
            w=$2
            h=$3
            x=$4
            y=$5
            if [[ "$( ${pkgs.file}/bin/file -Lb --mime-type "$file")" =~ ^image ]]; then
                ${pkgs.kitty}/bin/kitty +kitten icat --silent --stdin no --transfer-mode file --place "''${w}x''${h}@''${x}x''${y}" "$file" < /dev/null > /dev/tty
                exit 1
            fi
            ${pkgs.pistol}/bin/pistol "$file"
          '';
          cleaner = pkgs.writeShellScriptBin "clean.sh" ''
            ${pkgs.kitty}/bin/kitty +kitten icat --clear --stdin no --silent --transfer-mode file < /dev/null > /dev/tty
          '';
        in
        ''
          set cleaner ${cleaner}/bin/clean.sh
          set previewer ${previewer}/bin/pv.sh
        '';
    };
    mpv = {
      enable = true;
      scripts = with pkgs.mpvScripts; [
        thumbnail
        sponsorblock
        webtorrent-mpv-hook
      ];
    };
    qutebrowser = {
      enable = true;
      settings = {
        auto_save.session = true;
        colors.webpage.darkmode.enabled = true;
        tabs.position = "bottom";
        tabs.show = "always"; # always, switching, never
        statusbar.show = "in-mode";
      };
      extraConfig = "config.bind('<Ctrl+/>', 'hint links spawn --detach mpv --ytdl-format=bestvideo[height<=1080]+bestaudio/best[height<=1080] {hint-url}')";
      greasemonkey = [
        (pkgs.fetchurl {
          url = "https://raw.githubusercontent.com/afreakk/greasemonkeyscripts/master/youtube_adblock.js";
          sha256 = "sha256-AyD9VoLJbKPfqmDEwFIEBMl//EIV/FYnZ1+ona+VU9c=";
        })
        (pkgs.fetchurl {
          url = "https://raw.githubusercontent.com/afreakk/greasemonkeyscripts/master/youtube_sponsorblock.js";
          sha256 = "sha256-nwNade1oHP+w5LGUPJSgAX1+nQZli4Rhe8FFUoF5mLE=";
        })
      ];
    };
    ripgrep.enable = true;
    wezterm = {
      enable = true;
      enableZshIntegration = true;
      package = unstable.wezterm;
      extraConfig = ''
        return {
          window_background_opacity = 0.8,
          hide_tab_bar_if_only_one_tab = true,
          enable_wayland = false,
          keys = {
            { key = 't', mods = 'ALT', action = wezterm.action.ShowTabNavigator },
            { key = 'r', mods = 'ALT',
              action = wezterm.action.PromptInputLine {
                description = 'Enter new name for tab',
                action = wezterm.action_callback(function(window, pane, line)
                  if line then
                    window:active_tab():set_title(line)
                  end
                end),
              },
            },
            { key = 'h', mods = 'ALT',        action = wezterm.action.ActivatePaneDirection 'Left', },
            { key = 'l', mods = 'ALT',        action = wezterm.action.ActivatePaneDirection 'Right', },
            { key = 'j', mods = 'ALT',        action = wezterm.action.ActivatePaneDirection 'Down', },
            { key = 'k', mods = 'ALT',        action = wezterm.action.ActivatePaneDirection 'Up', },
            { key = 'h', mods = 'CTRL|SHIFT', action = wezterm.action.ActivateTabRelative(-1) },
            { key = 'l', mods = 'CTRL|SHIFT', action = wezterm.action.ActivateTabRelative(1) },
            { key = 'f', mods = 'ALT',        action = wezterm.action.TogglePaneZoomState },
          }
        }
      '';
    };
    yazi.enable = true;
    zathura = {
      enable = true;
      options = {
        # default-bg = "#1a1b26";
        # default-fg = "#9aa5ce";
        recolor = "true";
        # recolor-darkcolor = "#9aa5ce";
        # recolor-lightcolor = "#414868";
      };
    };
  };
  home.file.".local/bin/bookmarks" = {
    executable = true;
    text = ''
      #!/usr/bin/env bash
      ${pkgs.xdg-utils}/bin/xdg-open "$(grep -v '^#' ~/.local/share/bookmarks | ${pkgs.wofi}/bin/wofi -G --show dmenu | ${pkgs.gawk}/bin/awk '{print $1}')"
    '';
  };

  home.sessionVariables = {
    TERM = "xterm-256color";
    _ZL_DATA = "~/.cache/.zlua";
  };
}
