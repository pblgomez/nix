{ inputs, pkgs, ... }: {
  programs = {
    firefox = {
      enable = if pkgs.stdenv.isDarwin then false else true;
      policies = {
        AppAutoUpdate = false;
        AutofillAddressEnabled = false;
        AutofillCreditCardEnabled = false;
        DisablePocket = true;
        DisableFirefoxAccounts = true;
        DisableStudies = true;
        DisableTelemetry = true;
        FirefoxHome = {
          TopSites = false;
          SponsoredTopSites = false;
        };
        Homepage.StartPage = "previous-session";
        NoDefaultBookmarks = true;
        ManagedBookmarks = [{
          "name" = "NixOS";
          "children" = [
            {
              "name" = "Firefox NixOS wrapper";
              "url" =
                "https://github.com/NixOS/nixpkgs/blob/master/pkgs/applications/networking/browsers/firefox/wrapper.nix";
            }
            {
              "name" = "Firefox policies";
              "url" =
                "https://github.com/mozilla/policy-templates/blob/master/linux/policies.json";
            }
          ];
        }];
        OfferToSaveLogins = false;
        PasswordManagerEnabled = false;
        UserMessaging = {
          ExtensionRecommendations = false;
          SkipOnboarding = true;
        };
      };
    };
  };
  programs.firefox.profiles = {
    pbl = {
      id = 0;
      name = "pbl";
      search.default = "DuckDuckGo";
      search.force = true;
      settings = {
        "browser.ctrlTab.sortByRecentlyUsed" = true;
        "browser.tabs.firefox-view" = false;
        "extensions.autoDisableScopes" = 0;
      };
      containers = {
        "Personal" = {
          id = 1;
          color = "purple";
          icon = "fingerprint";
        };
        "Work Prod" = {
          id = 2;
          color = "red";
          icon = "briefcase";
        };
        "Work dev" = {
          id = 3;
          color = "green";
          icon = "briefcase";
        };
        "Shopping" = {
          id = 4;
          color = "orange";
          icon = "cart";
        };
        "Banking" = {
          id = 5;
          color = "pink";
          icon = "dollar";
        };
        "Social" = {
          id = 6;
          color = "blue";
          icon = "fence";
        };

      };
      extensions = with inputs.firefox-addons.packages.${pkgs.system}; [
        bitwarden
        # bypass-paywalls-clean  # TODO: Broken
        darkreader
        multi-account-containers
        tridactyl
        sponsorblock
        ublock-origin
        youtube-shorts-block
      ];

    };
  };
}
