{ config, pkgs, ... }:
{
  home = {
    packages = with pkgs; [ lua ];
  };

  programs.zsh = {
    enable = true;
    dotDir = ".config/zsh";
    autosuggestion.enable = true;
    enableCompletion = false; # It slows down as is alredy in per-user
    syntaxHighlighting.enable = true;
    history.path = "${config.xdg.dataHome}/zsh/history";
    shellAliases = {
      cat = "bat";

      g = "git";
      gde = "git checkout $(git symbolic-ref refs/remotes/origin/HEAD | awk -F 'origin/' '{print $2}')";
      gitcleanup = "git branch | grep -v $(git symbolic-ref refs/remotes/origin/HEAD | awk -F 'origin/' '{print $2}') | xargs git branch -D && git remote prune origin";
      glabmc = "${pkgs.glab}/bin/glab mr create --assignee pblgomez --fill --push --no-editor --remove-source-branch --squash-before-merge --yes";

      k = "kubecolor";
      kg = "k get";
      kd = "k describe";
      kns = "${pkgs.kubeswitch}/bin/switch ns";

      lst = "ls --tree";

      nn = "cd ~/Notes; nohup ${pkgs.git}/bin/git pull --rebase > /dev/null & disown; nvim";

      v = "nvim";
    };
    shellGlobalAliases = {
      G = "| grep";
      neat = "-o yaml | kubectl neat";
      glabmm = "${pkgs.glab}/bin/glab mr merge --auto-merge --remove-source-branch --squash --yes";
    };
    zplug = {
      enable = true;
      zplugHome = "${config.xdg.dataHome}/zplug";
      plugins = [
        { name = "skywind3000/z.lua"; }
        {
          name = "plugins/git-auto-fetch";
          tags = [ "from:oh-my-zsh" ];
        }
      ];
    };
    initExtra = ''
      bindkey -v  # Use vi mode

      command -v Hyprland >/dev/null && [ "$(tty)" = "/dev/tty1" ] && [ -z "$DISPLAY" ] && exec Hyprland

      setopt correct                                                                        # Auto correct mistakes
      setopt nocaseglob                                                                     # Ignore case
      setopt HIST_IGNORE_SPACE                                                              # Ignore commands starting with space
      zstyle -e ':completion:*' special-dirs '[[ $PREFIX = (../)#(|.|..) ]] && reply=(..)'  # Complete special dir not always ../
      export PATH="$HOME/.local/bin:$PATH"

      zstyle ':completion:*' menu select

      command -v kubecolor >/dev/null 2>&1 && alias kubectl="kubecolor" && compdef kubecolor=kubectl

      export PATH="$PATH:$HOME/.krew/bin"

      # command -v direnv >/dev/null && eval "$(direnv hook zsh)"

      [ -f /run/agenix/openai-api-key ] && export OPENAI_API_KEY="$(cat /run/agenix/openai-api-key)"
    '';
  };

  home.sessionVariables = {
    EDITOR = "nvim";
    TERM = "xterm-256color";
    _ZL_DATA = "~/.cache/.zlua";
  };
}
