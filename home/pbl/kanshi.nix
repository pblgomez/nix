{
  services.kanshi = {
    enable = true;
    systemdTarget = "hyprland-session.target";
    settings = [
      {
        profile.name = "docked";
        profile.outputs = [
          {
            criteria = "DP-1";
            position = "0,0";
          }
          {
            criteria = "HDMI-A-1";
            position = "2560,0";
            transform = "90";
          }
        ];
      }
      {
        profile.name = "docked_pre";
        profile.outputs = [
          {
            criteria = "DP-1";
            position = "0,0";
          }
          {
            criteria = "HDMI-A-1";
            position = "2560,0";
            transform = "90";
          }
          {
            criteria = "eDP-1";
            status = "disable";
          }
        ];
      }
      {
        profile.name = "laptop";
        profile.outputs = [{
          criteria = "eDP-1";
          position = "0,0";
          status = "enable";
        }];
      }
      {
        profile.name = "laptopext";
        profile.outputs = [
          {
            criteria = "DP-1";
            position = "0,0";
            status = "enable";
          }
          {
            criteria = "eDP-1";
            position = "600,1080";
            status = "enable";
          }
        ];
      }
    ];
  };
}
