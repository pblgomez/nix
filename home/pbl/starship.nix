{
  programs.starship = {
    enable = true;
    enableZshIntegration = true;
    settings = {
      format = "$all$custom$git_branch$git_status$line_break$battery$character";
      battery = {
        disabled = false;
        display = [
          {
            discharging_symbol = " 󰂎 ";
            style = "bold red";
            threshold = 20;
          }
          {
            discharging_symbol = "💦 ";
            style = "bold yellow";
            threshold = 30;
          }
        ];
      };
      custom.gitlab = {
        command = "git remote -v | grep gitlab";
        require_repo = true;
        symbol = " ";
        format = "$symbol";
        when = "git remote -v | grep gitlab";
      };
      custom.github = {
        command = "git remote -v | grep github";
        require_repo = true;
        symbol = " ";
        format = "$symbol";
        when = "git remote -v | grep github";
      };
      kubernetes = {
        disabled = false;
      };
      nix_shell = {
        disabled = false;
      };
    };
  };
}
