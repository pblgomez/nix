{ pkgs, ... }:
let
  terminal = "${pkgs.ghostty}/bin/ghostty";
  # terminal-spawn = cmd: "${terminal} cli spawn --new-window  -- ${cmd}";
  terminal-spawn = cmd: "${terminal} -e ${cmd}";
  # terminal-spawn = cmd: "${pkgs.kitty}/bin/kitty -e ${cmd}";

  bluetoothctlOn = "${pkgs.bluez}/bin/bluetoothctl power on";
  bluetoothctlOff = "${pkgs.bluez}/bin/bluetoothctl power off";
  blueman = "${pkgs.blueman}/bin/blueman-manager";

  volume = terminal-spawn pulsemixer;
  pulsemixer = "${pkgs.pulsemixer}/bin/pulsemixer";
in
{
  home.sessionVariables.NIXOS_OZONE_WL = "1";
  services = {
    cliphist.enable = true;
    hyprpaper.enable = true;
  };
  programs.waybar = {
    enable = true;
    package = pkgs.waybar.overrideAttrs (oldAttrs: {
      mesonFlags = oldAttrs.mesonFlags ++ [ "-Dexperimental=true" ];
    });
    settings = {
      primary = {
        # mode = "dock";
        layer = "top";
        modules-left = [ "hyprland/workspaces" ];
        "wlr/workspaces" = {
          on-click = "activate";
        };
        modules-right = [
          "cpu"
          "memory"
          "temperature"
          "backlight"
          "bluetooth"
          "pulseaudio"
          "battery"
          "network"
          "tray"
          "clock"
        ];
        cpu = {
          format = "󰻠  {usage}%";
        };
        memory = {
          format = "󰍛  {used}/{total}";
        };
        backlight = {
          format = "{percent}% {icon} ";
          format-icons = {
            default = [
              "󰃜"
              "󰃛"
              "󰃚"
            ];
          };
        };
        pulseaudio = {
          format = "{volume}% {icon} ";
          format-icons = {
            default = [
              ""
              ""
              ""
            ];
            headphone = "";
          };
          on-click = volume;
        };
        battery = {
          format = "{capacity}% {icon} ";
          format-icons = {
            default = [
              ""
              " "
              ""
              ""
              ""
            ];
          };
          tooltip = true;
          tooltip-format = "{timeTo} {power}W";
        };
        bluetooth = {
          format = "  {status} ";
          on-click = bluetoothctlOn;
          on-click-right = bluetoothctlOff;
          on-click-middle = blueman;
        };
        clock = {
          format = "{:%d/%m %H:%M}";
        };
      };
    };
    style = ''
      * {
          /* `otf-font-awesome` is required to be installed for icons */
          font-family: FontAwesome, Roboto, Helvetica, Arial, sans-serif;
          font-size: 13px;
      }

      window#waybar {
          background-color: rgba(43, 48, 59, 0.5);
          border-bottom: 3px solid rgba(100, 114, 125, 0.5);
          color: #ffffff;
          transition-property: background-color;
          transition-duration: .5s;
      }

      window#waybar.hidden {
          opacity: 0.2;
      }

      /*
      window#waybar.empty {
          background-color: transparent;
      }
      window#waybar.solo {
          background-color: #FFFFFF;
      }
      */

      window#waybar.termite {
          background-color: #3F3F3F;
      }

      window#waybar.chromium {
          background-color: #000000;
          border: none;
      }

      button {
          /* Use box-shadow instead of border so the text isn't offset */
          box-shadow: inset 0 -3px transparent;
          /* Avoid rounded borders under each button name */
          border: none;
          border-radius: 0;
      }

      /* https://github.com/Alexays/Waybar/wiki/FAQ#the-workspace-buttons-have-a-strange-hover-effect */
      button:hover {
          background: inherit;
          box-shadow: inset 0 -3px #ffffff;
      }

      #workspaces button {
          padding: 0 5px;
          background-color: transparent;
          color: #ffffff;
      }

      #workspaces button:hover {
          background: rgba(0, 0, 0, 0.2);
      }

      #workspaces button.active {
          background-color: #64727D;
          box-shadow: inset 0 -3px #ffffff;
      }

      #workspaces button.urgent {
          background-color: #eb4d4b;
      }

      #mode {
          background-color: #64727D;
          border-bottom: 3px solid #ffffff;
      }

      #clock,
      #battery,
      #cpu,
      #memory,
      #disk,
      #temperature,
      #backlight,
      #network,
      #pulseaudio,
      #wireplumber,
      #custom-media,
      #tray,
      #mode,
      #idle_inhibitor,
      #scratchpad,
      #mpd {
          padding: 0 10px;
          color: #ffffff;
      }

      #window,
      #workspaces {
          margin: 0 4px;
      }

      /* If workspaces is the leftmost module, omit left margin */
      .modules-left > widget:first-child > #workspaces {
          margin-left: 0;
      }

      /* If workspaces is the rightmost module, omit right margin */
      .modules-right > widget:last-child > #workspaces {
          margin-right: 0;
      }

      #clock {
          background-color: #64727D;
      }

      #battery {
          background-color: #ffffff;
          color: #000000;
      }

      #battery.charging, #battery.plugged {
          color: #ffffff;
          background-color: #26A65B;
      }

      @keyframes blink {
          to {
              background-color: #ffffff;
              color: #000000;
          }
      }

      #battery.critical:not(.charging) {
          background-color: #f53c3c;
          color: #ffffff;
          animation-name: blink;
          animation-duration: 0.5s;
          animation-timing-function: linear;
          animation-iteration-count: infinite;
          animation-direction: alternate;
      }

      label:focus {
          background-color: #000000;
      }

      #cpu {
          background-color: #2ecc71;
          color: #000000;
      }

      #memory {
          background-color: #9b59b6;
      }

      #disk {
          background-color: #964B00;
      }

      #backlight {
          background-color: #90b1b1;
      }

      #network {
          background-color: #2980b9;
      }

      #network.disconnected {
          background-color: #f53c3c;
      }

      #pulseaudio {
          background-color: #f1c40f;
          color: #000000;
      }

      #pulseaudio.muted {
          background-color: #90b1b1;
          color: #2a5c45;
      }

      #wireplumber {
          background-color: #fff0f5;
          color: #000000;
      }

      #wireplumber.muted {
          background-color: #f53c3c;
      }

      #custom-media {
          background-color: #66cc99;
          color: #2a5c45;
          min-width: 100px;
      }

      #custom-media.custom-spotify {
          background-color: #66cc99;
      }

      #custom-media.custom-vlc {
          background-color: #ffa000;
      }

      #temperature {
          background-color: #f0932b;
      }

      #temperature.critical {
          background-color: #eb4d4b;
      }

      #tray {
          background-color: #2980b9;
      }

      #tray > .passive {
          -gtk-icon-effect: dim;
      }

      #tray > .needs-attention {
          -gtk-icon-effect: highlight;
          background-color: #eb4d4b;
      }

      #idle_inhibitor {
          background-color: #2d3436;
      }

      #idle_inhibitor.activated {
          background-color: #ecf0f1;
          color: #2d3436;
      }

      #mpd {
          background-color: #66cc99;
          color: #2a5c45;
      }

      #mpd.disconnected {
          background-color: #f53c3c;
      }

      #mpd.stopped {
          background-color: #90b1b1;
      }

      #mpd.paused {
          background-color: #51a37a;
      }

      #language {
          background: #00b093;
          color: #740864;
          padding: 0 5px;
          margin: 0 5px;
          min-width: 16px;
      }

      #keyboard-state {
          background: #97e1ad;
          color: #000000;
          padding: 0 0px;
          margin: 0 5px;
          min-width: 16px;
      }

      #keyboard-state > label {
          padding: 0 5px;
      }

      #keyboard-state > label.locked {
          background: rgba(0, 0, 0, 0.2);
      }

      #scratchpad {
          background: rgba(0, 0, 0, 0.2);
      }

      #scratchpad.empty {
      	background-color: transparent;
      }
    '';
  };
  wayland.windowManager.hyprland = {
    enable = true;
    extraConfig = ''

      # For all categories, see https://wiki.hyprland.org/Configuring/Variables/
      input {
          kb_layout = us
          kb_variant = altgr-intl
          kb_model =
          kb_options =
          kb_rules =

          follow_mouse = 1

          touchpad {
              natural_scroll = yes
          }

          sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
      }

      general {
          # See https://wiki.hyprland.org/Configuring/Variables/ for more

          gaps_in = 3
          gaps_out = 5
          border_size = 2
          col.active_border = rgba(33ccffee) rgba(00ff99ee) 45deg
          col.inactive_border = rgba(595959aa)

      #   layout = dwindle
          layout = master

      }

      cursor {
        hide_on_key_press = true
        hide_on_touch = true
        inactive_timeout = 10
      }

      decoration {
          # See https://wiki.hyprland.org/Configuring/Variables/ for more

          rounding = 6
          blur {
              enabled = true
              size = 8
              passes = 1
              new_optimizations = true
            }
          inactive_opacity = 0.8
      }

      animations {
          enabled = yes

          # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

          bezier = myBezier, 0.05, 0.9, 0.1, 1.05

          animation = windows, 1, 7, myBezier
          animation = windowsOut, 1, 7, default, popin 80%
          animation = border, 1, 10, default
          animation = borderangle, 1, 8, default
          animation = fade, 1, 7, default
          animation = workspaces, 1, 6, default
      }

      dwindle {
          # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
          pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
          preserve_split = yes # you probably want this
      }

      master {
          # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
          # new_is_master = true
      }

      gestures {
          # See https://wiki.hyprland.org/Configuring/Variables/ for more
          workspace_swipe = on
      }

      # Example per-device config
      # See https://wiki.hyprland.org/Configuring/Keywords/#executing for more
      # device:epic-mouse-v1 {
      #     sensitivity = -0.5
      # }

      # Example windowrule v1
      # windowrule = float, ^(kitty)$
      # Example windowrule v2
      # windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
      # See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
      # windowrulev2 = dimaround,class:^Alacritty$
      windowrulev2 = workspace 2,class:^(firefox)
      windowrulev2 = workspace 5,class:^(Slack)


      # See https://wiki.hyprland.org/Configuring/Keywords/ for more
      $mainMod = SUPER

      # Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
      bind = $mainMod, Return, exec, ${terminal}
      # bind = $mainMod, Q, exec, kitty
      bind = $mainMod, q, killactive,
      bind = $mainMod SHIFT ALT, Q, exit,
      # bind = $mainMod, E, exec, dolphin
      bind = $mainMod Shift, f, togglefloating,
      bind = $mainMod, SPACE, exec, pkill wofi || ${pkgs.wofi}/bin/wofi -G --show drun
      bind = $mainMod, P, pseudo, # dwindle
      # bind = $mainMod, J, togglesplit, # dwindle

      # Move focus with mainMod + arrow keys
      bind = $mainMod, h, movefocus, l
      bind = $mainMod, l, movefocus, r
      bind = $mainMod, k, movefocus, u
      bind = $mainMod, j, movefocus, d

      # Switch workspaces with mainMod + [0-9]
      bind = $mainMod, 1, workspace, 1
      bind = $mainMod, 2, workspace, 2
      bind = $mainMod, 3, workspace, 3
      bind = $mainMod, 4, workspace, 4
      bind = $mainMod, 5, workspace, 5
      bind = $mainMod, 6, workspace, 6
      bind = $mainMod, 7, workspace, 7
      bind = $mainMod, 8, workspace, 8
      bind = $mainMod, 9, workspace, 9
      bind = $mainMod, 0, workspace, 10

      # Move active window to a workspace with mainMod + SHIFT + [0-9]
      bind = $mainMod SHIFT, 1, movetoworkspace, 1
      bind = $mainMod SHIFT, 2, movetoworkspace, 2
      bind = $mainMod SHIFT, 3, movetoworkspace, 3
      bind = $mainMod SHIFT, 4, movetoworkspace, 4
      bind = $mainMod SHIFT, 5, movetoworkspace, 5
      bind = $mainMod SHIFT, 6, movetoworkspace, 6
      bind = $mainMod SHIFT, 7, movetoworkspace, 7
      bind = $mainMod SHIFT, 8, movetoworkspace, 8
      bind = $mainMod SHIFT, 9, movetoworkspace, 9
      bind = $mainMod SHIFT, 0, movetoworkspace, 10

      # Scroll through existing workspaces with mainMod + scroll
      bind = $mainMod, mouse_down, workspace, e+1
      bind = $mainMod, mouse_up, workspace, e-1

      # Move/resize windows with mainMod + LMB/RMB and dragging
      bindm = $mainMod, mouse:272, movewindow
      bindm = $mainMod, mouse:273, resizewindow

      # Screen brightness
      bind=,XF86MonBrightnessDown,exec,brightnessctl set 5%-
      bind=,XF86MonBrightnessUp,exec,brightnessctl set +5%

      # bind = $mainMod ALT, B, exec, ~/.local/bin/bookmarks
      bind = $mainMod ALT, B, exec, $HOME/.local/bin/bookmarks

      # Screenshot
      bind = $mainMod ALT, S, exec, ${pkgs.grim}/bin/grim -g "$(${pkgs.slurp}/bin/slurp)" - | ${pkgs.imagemagick}/bin/convert - -shave 1x1 PNG:- | ${pkgs.swappy}/bin/swappy -f -

      # Clipboard
      bind = $mainMod ALT, C, exec, ${pkgs.cliphist}/bin/cliphist list | ${pkgs.wofi}/bin/wofi -G --dmenu --prompt "Copy item to clipboard" | ${pkgs.cliphist}/bin/cliphist decode | ${pkgs.wl-clipboard}/bin/wl-copy
      bind = $mainMod ALT, X, exec, ${pkgs.cliphist}/bin/cliphist list | ${pkgs.wofi}/bin/wofi -G --dmenu --prompt "Delete item"| ${pkgs.cliphist}/bin/cliphist delete

      # rbw
      bind = $mainMod ALT, p, submap, pass
      submap = pass
      bind = ,p, exec, RBW_PROFILE="pbl" ${pkgs.rofi-rbw}/bin/rofi-rbw --selector-args "\-G"
      bind = ,c, exec, RBW_PROFILE="pbl" ${pkgs.rofi-rbw}/bin/rofi-rbw --selector-args "\-G" --action copy
      bind = ,o, exec, RBW_PROFILE="veset" ${pkgs.rofi-rbw}/bin/rofi-rbw --selector-args "\-G"
      bind = ,escape,submap,reset
      submap=reset
      workspace = w[tv1], gapsout:0, gapsin:0
      workspace = f[1], gapsout:0, gapsin:0
    '';
    settings = {
      "$mod" = "SUPER";
      bind = [
        "$mod, F, fullscreen"
        "$mod SHIFT, Return, layoutmsg, swapwithmaster master"
      ];
      bindl = [
        ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
      ];
      bindle = [
        ", XF86AudioRaiseVolume, exec, wpctl set-volume -l 1.5 @DEFAULT_AUDIO_SINK@ 5%+"
        ", XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%-"
      ];
      env = [ "XCURSOR_SIZE,24" ];
      exec-once = [
        "${pkgs.waybar}/bin/waybar"
        "${pkgs.swayidle}/bin/swayidle -w timeout 300 '${pkgs.swaylock-effects}/bin/swaylock -f -S --effect-blur 10x3' timeout 600 'hyprctl dispatch dpms off eDP-1' resume 'hyprctl dispatch dpms on' before-sleep '${pkgs.swaylock-effects}/bin/swaylock -S --effect-blur 10x3'"
      ];
      monitor = ",preferred,auto,1";
      workspace = [
        "1, monitor:DP-1, on-created-empty:'${terminal}'"
        "2, monitor:DP-1, on-created-empty: 'firefox'"
        "3, monitor:DP-1, on-created-empty: 'qutebrowser'"
        "5, monitor:HDMI-1-1, on-created-empty: 'slack'"
      ];
    };
  };
}
