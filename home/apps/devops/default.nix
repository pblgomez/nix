{ inputs, pkgs, ... }:
let unstable = import inputs.nixpkgs-unstable { system = pkgs.system; };
in {
  home = {
    file.".kube/switch-config.yaml".text = ''
      ---
      kind: SwitchConfig
      version: v1alpha1
      kubeconfigStores:
        - kind: eks
          id: 1
          showPrefix: false
          config:
            profile: veset-dev-admin
            region: eu-west-1
        - kind: eks
          id: 2
          showPrefix: false
          config:
            profile: veset-prod-admin
            region: eu-west-1
        - kind: eks
          id: 3
          showPrefix: false
          config:
            profile: veset-prod-admin
            region: us-east-1
        - kind: eks
          id: 4
          showPrefix: false
          config:
            profile: veset-prod-admin
            region: ap-northeast-1
        - kind: eks
          id: 5
          showPrefix: false
          config:
            profile: veset-prod-admin
            region: ap-southeast-1
        - kind: eks
          id: 6
          showPrefix: false
          config:
            profile: veset-dev-admin
            region: eu-north-1
        - kind: eks
          id: 7
          showPrefix: false
          config:
            profile: veset-prod-admin
            region: ap-northeast-1
        - kind: eks
          id: 8
          showPrefix: false
          config:
            profile: veset-prod-admin
            region: ap-southeast-1
        - kind: eks
          id: 9
          showPrefix: false
          config:
            profile: veset-dev-admin
            region: us-east-1
        - kind: eks
          id: 1j
          showPrefix: false
          config:
            profile: veset-prod-admin
            region: us-east-1
    '';
    packages = with pkgs; [
      awscli2
      dogdns
      eksctl
      unstable.glab

      kubectl
      kubecolor
      kubernetes-helm
      krew
      k9s

      stern
      sops
      xh
      vault
    ];
    sessionVariables = {
      AWS_SHARED_CREDENTIALS_FILE = "$HOME/.config/aws/credentials";
      AWS_CONFIG_FILE = "$HOME/.config/aws/config";
    };
  };
  programs.zsh = {
    shellAliases = {
      kk = ''
        output=$(${pkgs.kubeswitch}/bin/switch) && export KUBECONFIG="$(echo $output | awk -F'[ ,]' '{print $2}')"'';
    };
  };
}
