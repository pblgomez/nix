{ inputs, pkgs, ... }:
let unstable = import inputs.nixpkgs-unstable { system = pkgs.system; };
in {
  imports = [ ../neovim ../lf.nix ../zellij.nix ../zsh.nix ../starship.nix ];

  programs = {
    bat.enable = true;
    direnv = {
      enable = true;
      enableZshIntegration = true; # see note on other shells below
      nix-direnv.enable = true;
    };
    git = {
      enable = true;
      userName = "Pablo Gómez";
      userEmail = "pablogomez@pablogomez.com";
      signing = { key = "E33576E61E060DAD"; };
      aliases = {
        aliases =
          "!git config --list | grep 'alias\\.' | sed 's/alias\\.\\([^=]*\\)=\\(.*\\)/\\1\\ 	 => \\2/' | sort";
        a = "add";
        b = "branch";
        c = "commit";
        ci = "commit";
        co = "checkout";
        cob = "checkout -b";
        l =
          "log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit";
        pl = "pull";
        s = "status --short";
      };
      extraConfig = {
        init = { defaultBranch = "main"; };
        core = { excludefile = "$HOME/.config/git/gitignore"; };
        push = { autoSetupRemote = true; };
      };
    };
    home-manager.enable = true;
    kitty = {
      enable = true;
      shellIntegration.enableZshIntegration = true;
      settings.confirm_os_window_close = 0;
    };
    qutebrowser = {
      enable = true;
      package = unstable.qutebrowser;
      settings = {
        auto_save.session = true;
        colors.webpage.darkmode.enabled = true;
        tabs.position = "bottom";
        tabs.show = "always"; # always, switching, never
        statusbar.show = "in-mode";
      };
      extraConfig =
        "\nconfig.bind('<Ctrl+/>', 'hint links spawn --detach mpv {hint-url}')\n      ";
      greasemonkey = [
        (pkgs.fetchurl {
          url =
            "https://raw.githubusercontent.com/afreakk/greasemonkeyscripts/master/youtube_adblock.js";
          sha256 = "sha256-m8hplisKCiPnTd6aERTmgeGh2DcqOq6wfG18q90kwbA=";
        })
        (pkgs.fetchurl {
          url =
            "https://raw.githubusercontent.com/afreakk/greasemonkeyscripts/master/youtube_sponsorblock.js";
          sha256 = "sha256-e3QgDPa3AOpPyzwvVjPQyEsSUC9goisjBUDMxLwg8ZE=";
        })
      ];
    };
  };
}
