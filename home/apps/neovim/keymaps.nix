{
  programs.nixvim = {
    keymaps = [
      # Better Navigation
      {
        action = "<C-w>h";
        key = "<C-h>";
      }
      {
        action = "<C-w>j";
        key = "<C-j>";
      }
      {
        action = "<C-w>l";
        key = "<C-l>";
      }
      {
        action = "<C-w>l";
        key = "<C-l>";
      }

      # Navigate buffers
      {
        action = ":bnext<CR>";
        key = "<S-l>";
        options = {silent = true;};
      }
      {
        action = ":bprevious<CR>";
        key = "<S-h>";
        options = {silent = true;};
      }
      {
        action = ":bdelete<CR>";
        key = "<S-x>";
        options = {silent = true;};
      }

      # Move paragraphs
      {
        action = ":m '>+1<CR>gv=gv";
        key = "J";
      }
      {
        action = ":m '<-2<CR>gv=gv";
        key = "K";
      }

      # Stay in indent mode
      {
        mode = "v";
        action = "<gv";
        key = "<";
      }
      {
        mode = "v";
        action = ">gv";
        key = ">";
      }

      {
        # Keep cursor in the same place when useing 'J'
        mode = "n";
        action = "mzJ`z";
        key = "J";
      }

      # Paste without yank
      {
        action = "\"_dp";
        key = "<leader>p";
        mode = "x";
      }
      # Delete without yank
      {
        mode = ["n" "v"];
        action = "\"_d";
        key = "<leader>d";
      }

      # Yank to clipboard
      {
        mode = ["n" "v"];
        action = "\"+y";
        key = "<leader>y";
      }
      {
        mode = ["n" "v"];
        action = "\"+Y";
        key = "<leader>Y";
      }

      # Oil
      {
        action = "<cmd>:Oil<CR>";
        key = "<leader>e";
      }
      {
        action = "<cmd>:Oil .<CR>";
        key = "<leader>E";
      }

      # Project_nvim
      {
        action = "<cmd>:Telescope projects<CR>";
        key = "<leader>fp";
      }
    ];
  };
}
