{
  inputs,
  pkgs,
  ...
}: let
  unstable = import inputs.nixpkgs-unstable {system = pkgs.system;};
in {
  programs.nixvim = {
    plugins = {
      markdown-preview.enable = true;
      markdown-preview.browser = "${unstable.qutebrowser}/bin/qutebrowser";
    };
    keymaps = [
      {
        action = ":MarkdownPreviewToggle<CR>";
        key = "<leader>m";
      }
    ];
  };
}
