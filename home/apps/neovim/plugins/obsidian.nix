{
  pkgs,
  inputs,
  ...
}: {
  nixpkgs = {
    overlays = [
      (final: prev: {
        vimPlugins =
          prev.vimPlugins
          // {
            obsidian = prev.vimUtils.buildVimPlugin {
              name = "obsidian";
              src = inputs.nvim-plugin-obsidian;
            };
          };
      })
    ];
  };
  programs.nixvim = {
    extraPlugins = with pkgs.vimPlugins; [
      {
        plugin = obsidian;
        config =
          /*
          lua
          */
          ''
            lua require("obsidian").setup({workspaces = {{name = "Notes", path="~/Notes",},},})
          '';
      }
    ];
    keymaps = [
      {
        action = "<cmd>ObsidianQuickSwitch<CR>";
        key = "<leader>ns";
      }
    ];
  };
}
