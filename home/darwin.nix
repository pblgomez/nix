{inputs, ...}: {
  imports = [
    ./apps/common
    inputs.nixvim.homeManagerModules.nixvim
  ];
  home.stateVersion = "23.11";
}
