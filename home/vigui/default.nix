{ pkgs, ... }:
{
  programs = {
    chromium = {
      enable = true;
      dictionaries = [ pkgs.hunspellDictsChromium.en_US ];
      extensions = [
        { id = "cjpalhdlnbpafiamejdnhcphjbkeiagm"; } # ublock-origin
        { id = "nngceckbapebfimnlniiiahkandclblb"; } # bitwarden
      ];
    };
  };
  home.stateVersion = "24.11";
}
