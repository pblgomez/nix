let
  pbl-infinity = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIEoe8CHQx53FwXs7SxKoYb8qgivXEza6vdIobOOwsYtP";
  pbl-mbp183 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIG6CUCfrQGYHpT6Sg0zSTAM/Wl0rUrBJkGvO2fomIdvA";
  pblAsahi = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHLybgFFdcCz5NYRRGF5dtGFeRh2qpIdll+S/w40gRSR";
  pbl-darwintest = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKYSS/1SQ7PqA6VKHjJn8uNp4eK77EDDj7CtC2cCjoSM";
  root-n100-1 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPRGw0Q90vblx0OHAJTk8iMUrEqgmMaVeRTYxDRTJoU2";
  pbl-connectbot-pixel6 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFPwyUa5MPvY/9Q36hrSqosB25VOFuHcgOi4gQSpO0l1";
  users = [
    pbl-infinity
    pbl-mbp183
    pblAsahi
    pbl-darwintest
    pbl-connectbot-pixel6
    root-n100-1
  ];

  # From /etc/ssh
  asuspro5dip-1 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDYlrmr6PN8aPU5uR72yEQXxjEOsqOaWhzgViwiH3GRV";
  infinity = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAMM08h/vahpHfvjGoIEwzyFGaEzW64y216LG55iY8gg";
  nixos3 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKNfsh+11G5ku0Ork9cR5rZqlCR8F2iWWS1DOOPmF+to";
  n100-1 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOUbir3yqKoTXR9satuBG+G1oYHb5VCk16H4oBamvPkg";
  rpi4-2 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB/jv8pND3tGjkDIk+qe9Q1Jrl6nms4LIAzVTdjVz7Kn";
  proxmox-nixos-3 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIMtJIyTCUGJE4+t/OHi+gEH633SnodAQDwA2F96yUao7";
  proxmox-nixos-master-1 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIF7Lbm/CZwTFdo/jfW/PpVNWr55KFIB8NjFvK9iEI56Z";
  proxmox-nixos-master-2 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIABlbqJsltFVIOwHz5DmuKdI8kWoHysceNlZGkQW+Lrn";
  proxmox-nixos-worker-1 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIOZ1ArJXJAlXdDBqw8GWnJAzK0XIhO348tfLn1oQb7Wa";
  proxmox-nixos-worker-2 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIPHTceckgZweSmQuYC18NdqZlJkqjjzFJPUo3KV4Pf24";
  proxmox-nixos-worker-3 = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBrYrp1DaoGzsf6ETLDByU6Wxg5HtT/lx2rfwNLu/Eh4";
  systems = [
    asuspro5dip-1
    infinity
    nixos3
    n100-1
    proxmox-nixos-worker-1
    proxmox-nixos-3
    proxmox-nixos-master-1
    proxmox-nixos-master-2
    proxmox-nixos-worker-2
    proxmox-nixos-worker-3
    rpi4-2
  ];
in
{
  "secrets/age.age".publicKeys = users ++ systems;
  "secrets/awsCreds.age".publicKeys = users ++ systems;
  "secrets/bookmarks.age".publicKeys = users ++ systems;
  "secrets/bwAddress.age".publicKeys = users ++ systems;
  "secrets/flux-glab-pat.age".publicKeys = users ++ systems;
  "secrets/email.age".publicKeys = users ++ systems;
  "secrets/email-veset.age".publicKeys = users ++ systems;
  "secrets/k3sToken.age".publicKeys = users ++ systems;
  "secrets/sshConfig.age".publicKeys = users ++ systems;
  "secrets/sshHostKey-asuspro5dip.age".publicKeys = users ++ systems;
  "secrets/sshHostKey-rpi4-2.age".publicKeys = users ++ systems;
  "secrets/sshHostKey-infinity.age".publicKeys = users ++ systems;
  "secrets/sshHostKey-n100-1.age".publicKeys = users ++ systems;
  "secrets/sshHostKeyPublic-n100-1.age".publicKeys = users ++ systems;
  "secrets/sshKey-n100-1.age".publicKeys = users ++ systems;
  "secrets/sshKey-infinity-pbl.age".publicKeys = users ++ systems;
  "secrets/tailscaleToken.age".publicKeys = users ++ systems;
  "secrets/mqtt-password.age".publicKeys = users ++ systems;
  "secrets/openai-api-key.age".publicKeys = users ++ systems;
}
